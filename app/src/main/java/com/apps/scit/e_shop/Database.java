package com.apps.scit.e_shop;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.apps.scit.e_shop.Model.Category;
import com.apps.scit.e_shop.Model.ConfigMainObject;
import com.apps.scit.e_shop.Model.ConfigObject;
import com.apps.scit.e_shop.Model.Notification;
import com.apps.scit.e_shop.Model.Offer;
import com.apps.scit.e_shop.Model.Product;
import com.apps.scit.e_shop.Model.ProductImage;
import com.apps.scit.e_shop.Model.ProfileObject;
import com.apps.scit.e_shop.Model.SliderObject;
import com.apps.scit.e_shop.Model.SpecialOfferImage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 7/23/2018.
 */

public class Database {
    private static DB database;
    public Database(Context context){
        database = new DB(context);
    }
    //Insert Values---------------------------------------------------------------------------------
    public long insertConfig(int id,String name,String unique_name){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.Configs_id,id);
        contentvalues.put(database.Configs_name,name);
        contentvalues.put(database.Configs_unique_name,unique_name);
        long result = sqlitedatabase.insert(database.Configs,null,contentvalues);
        return result;
    }
    public long insertConfiguration(int id,int config_id,String name,String value){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.Configurations_id,id);
        contentvalues.put(database.Configurations_config_id,config_id);
        contentvalues.put(database.Configurations_name,name);
        contentvalues.put(database.Configurations_value,value);
        long result = sqlitedatabase.insert(database.Configurations,null,contentvalues);
        return result;
    }
    public long insertSlider(int id,String title,String image,int target,String url){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.Slider_id,id);
        contentvalues.put(database.Slider_title,title);
        contentvalues.put(database.Slider_image,image);
        contentvalues.put(database.Slider_target,target);
        contentvalues.put(database.Slider_url,url);
        long result = sqlitedatabase.insert(database.Slider,null,contentvalues);
        return result;
    }
    public long insertCategory(int id,String name,String image,String icon_image,int parent_id){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.Categories_id,id);
        contentvalues.put(database.Categories_name,name);
        contentvalues.put(database.Categories_image,image);
        contentvalues.put(database.Categories_icon_image,icon_image);
        contentvalues.put(database.Categories_parent_id,parent_id);
        long result = sqlitedatabase.insert(database.Categories,null,contentvalues);
        return result;
    }
    public long insertProduct(int id,String name,String image,String description,int price,String avgRate,int offer_price,int is_offer,String category_name){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.Products_id,id);
        contentvalues.put(database.Products_name,name);
        contentvalues.put(database.Products_image,image);
        contentvalues.put(database.Products_description,description);
        contentvalues.put(database.Products_price,price);
        contentvalues.put(database.Products_avgRate,avgRate);
        contentvalues.put(database.Products_offer_price,offer_price);
        contentvalues.put(database.Products_is_offer,is_offer);
        contentvalues.put(database.Products_category_name,category_name);
        long result = sqlitedatabase.insert(database.Products,null,contentvalues);
        return result;
    }
    public long insertProductImage(int id,String image,int product_id){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.ProductImages_id,id);
        contentvalues.put(database.ProductImages_image,image);
        contentvalues.put(database.ProductImages_product_id,product_id);
        long result = sqlitedatabase.insert(database.ProductImages,null,contentvalues);
        return result;
    }
    public long insertNormalOffer(int id,String name,String image,String description,int price,String category_name,String avgRate,int offer_price,int is_offer,String offer_remain){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.NormalOffers_id,id);
        contentvalues.put(database.NormalOffers_name,name);
        contentvalues.put(database.NormalOffers_image,image);
        contentvalues.put(database.NormalOffers_description,description);
        contentvalues.put(database.NormalOffers_price,price);
        contentvalues.put(database.NormalOffers_category_name,category_name);
        contentvalues.put(database.NormalOffers_avgRate,avgRate);
        contentvalues.put(database.NormalOffers_offer_price,offer_price);
        contentvalues.put(database.NormalOffers_is_offer,is_offer);
        contentvalues.put(database.NormalOffers_offer_remain,offer_remain);
        long result = sqlitedatabase.insert(database.NormalOffers,null,contentvalues);
        return result;
    }
    public long insertSpecialOffer(int id,String information,String image_url,String start_date,String end_date,int offer_price,int price,String offer_remain){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.SpecialOffers_id,id);
        contentvalues.put(database.SpecialOffers_information,information);
        contentvalues.put(database.SpecialOffers_image_url,image_url);
        contentvalues.put(database.SpecialOffers_start_date,start_date);
        contentvalues.put(database.SpecialOffers_end_date,end_date);
        contentvalues.put(database.SpecialOffers_offer_price,offer_price);
        contentvalues.put(database.SpecialOffers_price,price);
        contentvalues.put(database.SpecialOffers_offer_remain,offer_remain);
        long result = sqlitedatabase.insert(database.SpecialOffers,null,contentvalues);
        return result;
    }
    public long insertSpecialOfferProduct(int id,String name,String image,String description,int price,String category_name,int is_offer,int offer_price,String avgRate,int offer_id){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.SpecialOfferProducts_id,id);
        contentvalues.put(database.SpecialOfferProducts_name,name);
        contentvalues.put(database.SpecialOfferProducts_image,image);
        contentvalues.put(database.SpecialOfferProducts_description,description);
        contentvalues.put(database.SpecialOfferProducts_price,price);
        contentvalues.put(database.SpecialOfferProducts_category_name,category_name);
        contentvalues.put(database.SpecialOfferProducts_is_offer,is_offer);
        contentvalues.put(database.SpecialOfferProducts_offer_price,offer_price);
        contentvalues.put(database.SpecialOfferProducts_avgRate,avgRate);
        contentvalues.put(database.SpecialOfferProducts_offer_id,offer_id);
        long result = sqlitedatabase.insert(database.SpecialOfferProducts,null,contentvalues);
        return result;
    }
    public long insertSpecialOfferImage(int id,int offer_id,String image_url){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.SpecialOfferImages_id,id);
        contentvalues.put(database.SpecialOfferImages_offer_id,offer_id);
        contentvalues.put(database.SpecialOfferImages_image_url,image_url);
        long result = sqlitedatabase.insert(database.SpecialOfferImages,null,contentvalues);
        return result;
    }
    public long insertTopSearchedProduct(int id,String name,String image,String description,int price,String category_name,String avgRate,int offer_price,int is_offer){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.TopSearchedProducts_id,id);
        contentvalues.put(database.TopSearchedProducts_name,name);
        contentvalues.put(database.TopSearchedProducts_image,image);
        contentvalues.put(database.TopSearchedProducts_description,description);
        contentvalues.put(database.TopSearchedProducts_price,price);
        contentvalues.put(database.TopSearchedProducts_category_name,category_name);
        contentvalues.put(database.TopSearchedProducts_avgRate,avgRate);
        contentvalues.put(database.TopSearchedProducts_offer_price,offer_price);
        contentvalues.put(database.TopSearchedProducts_is_offer,is_offer);
        long result = sqlitedatabase.insert(database.TopSearchedProducts,null,contentvalues);
        return result;
    }
    public long insertTopRatedProduct(int id,String name,String image,String description,int price,String category_name,String avgRate,int offer_price,int is_offer){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.TopRatedProducts_id,id);
        contentvalues.put(database.TopRatedProducts_name,name);
        contentvalues.put(database.TopRatedProducts_image,image);
        contentvalues.put(database.TopRatedProducts_description,description);
        contentvalues.put(database.TopRatedProducts_price,price);
        contentvalues.put(database.TopRatedProducts_category_name,category_name);
        contentvalues.put(database.TopRatedProducts_avgRate,avgRate);
        contentvalues.put(database.TopRatedProducts_offer_price,offer_price);
        contentvalues.put(database.TopRatedProducts_is_offer,is_offer);
        long result = sqlitedatabase.insert(database.TopRatedProducts,null,contentvalues);
        return result;
    }
    public long insertTopViewedProduct(int id,String name,String image,String description,int price,String category_name,String avgRate,int offer_price,int is_offer){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.TopViewedProducts_id,id);
        contentvalues.put(database.TopViewedProducts_name,name);
        contentvalues.put(database.TopViewedProducts_image,image);
        contentvalues.put(database.TopViewedProducts_description,description);
        contentvalues.put(database.TopViewedProducts_price,price);
        contentvalues.put(database.TopViewedProducts_category_name,category_name);
        contentvalues.put(database.TopViewedProducts_avgRate,avgRate);
        contentvalues.put(database.TopViewedProducts_offer_price,offer_price);
        contentvalues.put(database.TopViewedProducts_is_offer,is_offer);
        long result = sqlitedatabase.insert(database.TopViewedProducts,null,contentvalues);
        return result;
    }
    public long insertProfile(String token,int product_notification_flag,int offer_notification_flag,String mobile_number,int id){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.Profile_token,token);
        contentvalues.put(database.Profile_product_notification_flag,product_notification_flag);
        contentvalues.put(database.Profile_offer_notification_flag,offer_notification_flag);
        contentvalues.put(database.Profile_mobile_number,mobile_number);
        contentvalues.put(database.Profile_id,id);
        long result = sqlitedatabase.insert(database.Profile,null,contentvalues);
        return result;
    }
    public long insertNotification(String text,String date){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.Notifications_text,text);
        contentvalues.put(database.Notifications_date,date);
        long result = sqlitedatabase.insert(database.Notifications,null,contentvalues);
        return result;
    }
    //get Rows-------------------------------------------------------------------------------------------
    public List<ConfigMainObject> getConfigs(){
        List<ConfigMainObject> list = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.Configs_id,database.Configs_name,database.Configs_unique_name};
        Cursor cursor = sqlitedatabase.query(database.Configs,columns,null,null,null,null,null);
        while(cursor.moveToNext()){
            list.add(new ConfigMainObject(cursor.getInt(0),cursor.getString(1),cursor.getString(2)));
        }
        return list;
    }
    public List<ConfigObject> getConfigurations(int config_id){
        List<ConfigObject> list = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.Configurations_id,database.Configurations_config_id,database.Configurations_name,database.Configurations_value};
        Cursor cursor = sqlitedatabase.query(database.Configurations,columns,database.Configurations_config_id+"='"+config_id+"'",null,null,null,null);
        while(cursor.moveToNext()){
            list.add(new ConfigObject(cursor.getInt(0),cursor.getInt(1),cursor.getString(2),cursor.getString(3)));
        }
        return list;
    }
    public List<SliderObject> getSliders(){
        List<SliderObject> list = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.Slider_id,database.Slider_title,database.Slider_image,database.Slider_target,database.Slider_url};
        Cursor cursor = sqlitedatabase.query(database.Slider,columns,null,null,null,null,null);
        while(cursor.moveToNext()){
            list.add(new SliderObject(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getInt(3),cursor.getString(4)));
        }
        return list;
    }
    public List<Category> getCategories(int parent_id){
        List<Category> list = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.Categories_id,database.Categories_name,database.Categories_image,database.Categories_icon_image,database.Categories_parent_id};
        Cursor cursor = sqlitedatabase.query(database.Categories,columns,database.Categories_parent_id+"='"+parent_id+"'",null,null,null,null);
        while(cursor.moveToNext()){
            list.add(new Category(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getInt(4)));
        }
        return list;
    }
    public Category getSingleCategory(int id){
        Category category = new Category();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.Categories_id,database.Categories_name,database.Categories_image,database.Categories_icon_image,database.Categories_parent_id};
        Cursor cursor = sqlitedatabase.query(database.Categories,columns,database.Categories_id+"='"+id+"'",null,null,null,null);
        while(cursor.moveToNext()) {
            category.setId(cursor.getInt(0));
            category.setName(cursor.getString(1));
            category.setImage(cursor.getString(2));
            category.setIcon_image(cursor.getString(3));
            //category.setParent_id(cursor.getInt(4));
        }
        return category;
    }
    public int getCategoryParent(int category_id){
        int parent = 0;
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.Categories_parent_id};
        Cursor cursor = sqlitedatabase.query(database.Categories,columns,database.Categories_id+"='"+category_id+"'",null,null,null,null);
        while(cursor.moveToNext()){
            parent = cursor.getInt(0);
        }
        return parent;
    }
    public List<Product> getProducts(){
        List<Product> list = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.Products_id,database.Products_name,database.Products_image,database.Products_description
                ,database.Products_price,database.Products_avgRate,database.Products_offer_price,database.Products_is_offer
                ,database.Products_category_name};
        Cursor cursor = sqlitedatabase.query(database.Products,columns,null,null,null,null,null);
        while(cursor.moveToNext()){
            list.add(new Product(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                    cursor.getInt(4),cursor.getString(5),cursor.getInt(6),cursor.getInt(7),cursor.getString(8)));
        }
        return list;
    }
    public Product getSingleProduct(int product_id){
        Product product = new Product();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.Products_id,database.Products_name,database.Products_image,database.Products_description
                ,database.Products_price,database.Products_avgRate,database.Products_offer_price,database.Products_is_offer
                ,database.Products_category_name,database.Products_available};
        Cursor cursor = sqlitedatabase.query(database.Products,columns,database.Products_id+"='"+product_id+"'",null,null,null,null);
        //todo set Available
        while (cursor.moveToNext()){
            product.setId(cursor.getInt(0));
            product.setName(cursor.getString(1));
            product.setImage(cursor.getString(2));
            product.setDescription(cursor.getString(3));
            product.setPrice(cursor.getInt(4));
            product.setAvgRate(cursor.getString(5));
            product.setOffer_price(cursor.getInt(6));
            product.setIs_offer(cursor.getInt(7));
            product.setCategory_name(cursor.getString(8));
            product.setAvailable(cursor.getInt(9));
        }
        return product;
    }
    public int getProductIsRated(int product_id){
        int rate = 0;
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.Products_is_rated};
        Cursor cursor = sqlitedatabase.query(database.Products,columns,database.Products_id+" = "+product_id,null,null,null,null);
        while(cursor.moveToNext()){
            rate = cursor.getInt(0);
        }
        return rate;
    }
    public List<ProductImage> getProductImages(int product_id){
        List<ProductImage> listOfImages = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.ProductImages_id,database.ProductImages_image,database.ProductImages_product_id};
        Cursor cursor = sqlitedatabase.query(database.ProductImages,columns,database.ProductImages_product_id+" = "+product_id,null,null,null,null);
        while (cursor.moveToNext()){
            listOfImages.add(new ProductImage(cursor.getInt(0),cursor.getString(1),cursor.getInt(2)));
        }
        return listOfImages;
    }
    public List<Product> getNormalOffers(){
        List<Product> list = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.NormalOffers_id,database.NormalOffers_name,database.NormalOffers_image,
                database.NormalOffers_description,database.NormalOffers_price,database.NormalOffers_avgRate,
                database.NormalOffers_offer_price,database.NormalOffers_is_offer,database.NormalOffers_category_name,database.NormalOffers_offer_remain};
        Cursor cursor = sqlitedatabase.query(database.NormalOffers,columns,null,null,null,null,null);
        while(cursor.moveToNext()){
            list.add(new Product(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                    cursor.getInt(4),cursor.getString(5),cursor.getInt(6),cursor.getInt(7),cursor.getString(8),cursor.getInt(9)));
        }
        return list;
    }
    public Product getSingleNormalOffer(int id){
        Product product = new Product();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.NormalOffers_id,database.NormalOffers_name,database.NormalOffers_image,database.NormalOffers_description
                ,database.NormalOffers_price,database.NormalOffers_avgRate,database.NormalOffers_offer_price,database.NormalOffers_is_offer
                ,database.NormalOffers_category_name,database.NormalOffers_available};
        Cursor cursor = sqlitedatabase.query(database.NormalOffers,columns,database.NormalOffers_id+"='"+id+"'",null,null,null,null);
        //todo Set available
        while(cursor.moveToNext()){
            product.setId(cursor.getInt(0));
            product.setName(cursor.getString(1));
            product.setImage(cursor.getString(2));
            product.setDescription(cursor.getString(3));
            product.setPrice(cursor.getInt(4));
            product.setAvgRate(cursor.getString(5));
            product.setOffer_price(cursor.getInt(6));
            product.setIs_offer(cursor.getInt(7));
            product.setCategory_name(cursor.getString(8));
            product.setAvailable(cursor.getInt(9));
        }
        return product;
    }
    public int getNormalOfferIsRated(int offer_id){
        int is_rated = 0;
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.NormalOffers_is_rated};
        Cursor cursor = sqlitedatabase.query(database.NormalOffers,columns,database.NormalOffers_id+" = "+offer_id,null,null,null,null);
        while(cursor.moveToNext()){
            is_rated = cursor.getInt(0);
        }
        return is_rated;
    }
    public List<Offer> getSpecialOffers(){
        List<Offer> list = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.SpecialOffers_id,database.SpecialOffers_information,database.SpecialOffers_image_url,database.SpecialOffers_start_date,
                database.SpecialOffers_end_date,database.SpecialOffers_offer_price,database.SpecialOffers_price,database.SpecialOffers_offer_remain};
        Cursor cursor = sqlitedatabase.query(database.SpecialOffers,columns,null,null,null,null,null);
        while(cursor.moveToNext()){
            list.add(new Offer(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),
                    cursor.getInt(5),cursor.getInt(6),cursor.getString(7)));
        }
        return list;
    }
    public Offer getSingleSpecialOffer(int offer_id){
        Offer offer = new Offer();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.SpecialOffers_id,database.SpecialOffers_information,database.SpecialOffers_image_url,database.SpecialOffers_start_date,
                database.SpecialOffers_end_date,database.SpecialOffers_offer_price,database.SpecialOffers_price,database.SpecialOffers_offer_remain};
        Cursor cursor = sqlitedatabase.query(database.SpecialOffers,columns,database.SpecialOffers_id+"='"+offer_id+"'",null,null,null,null);
        while (cursor.moveToNext()){
            offer.setId(cursor.getInt(0));
            offer.setInformation(cursor.getString(1));
            offer.setImage_url(cursor.getString(2));
            offer.setStart_date(cursor.getString(3));
            offer.setEnd_date(cursor.getString(4));
            offer.setOffer_price(cursor.getInt(5));
            offer.setPrice(cursor.getInt(6));
            offer.setOffer_remain(cursor.getString(7));
        }
        return offer;
    }
    public List<Product> getSpecialOfferProducts(int offer_id){
        List<Product> list = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.SpecialOfferProducts_id,database.SpecialOfferProducts_name,database.SpecialOfferProducts_image,
                database.SpecialOfferProducts_description,database.SpecialOfferProducts_price,database.SpecialOfferProducts_category_name,
                database.SpecialOfferProducts_is_offer,database.SpecialOfferProducts_offer_price,database.SpecialOfferProducts_avgRate};
        Cursor cursor = sqlitedatabase.query(database.SpecialOfferProducts,columns,database.SpecialOfferProducts_offer_id+"='"+offer_id+"'",null,null,null,null);
        while(cursor.moveToNext()){
            list.add(new Product(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                    cursor.getInt(4),cursor.getString(8),cursor.getInt(7),cursor.getInt(6),cursor.getString(5)));
        }
        return list;
    }
    public List<SpecialOfferImage> getSpecialOfferImages(int offer_id){
        List<SpecialOfferImage> list = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.SpecialOfferImages_id,database.SpecialOfferImages_offer_id,database.SpecialOfferImages_image_url};
        Cursor cursor = sqlitedatabase.query(database.SpecialOfferImages,columns,database.SpecialOfferImages_offer_id+"='"+offer_id+"'",null,null,null,null);
        while(cursor.moveToNext()){
            list.add(new SpecialOfferImage(cursor.getInt(0),cursor.getInt(1),cursor.getString(2)));
        }
        return list;
    }
    public List<Product> getTopSearchedProducts(){
        List<Product> list = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.TopSearchedProducts_id,database.TopSearchedProducts_name,database.TopSearchedProducts_image,
                database.TopSearchedProducts_description,database.TopSearchedProducts_price,database.TopSearchedProducts_category_name,
                database.TopSearchedProducts_avgRate,database.TopSearchedProducts_offer_price,database.TopSearchedProducts_is_offer};
        Cursor cursor = sqlitedatabase.query(database.TopSearchedProducts,columns,null,null,null,null,null);
        while(cursor.moveToNext()){
            list.add(new Product(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                    cursor.getInt(4),cursor.getString(6),cursor.getInt(7),cursor.getInt(8),cursor.getString(5)));
        }
        return list;
    }
    public List<Product> getTopRatedProducts(){
        List<Product> list = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.TopRatedProducts_id,database.TopRatedProducts_name,database.TopRatedProducts_image,
                database.TopRatedProducts_description,database.TopRatedProducts_price,database.TopRatedProducts_category_name,
                database.TopRatedProducts_avgRate,database.TopRatedProducts_offer_price,database.TopRatedProducts_is_offer};
        Cursor cursor = sqlitedatabase.query(database.TopRatedProducts,columns,null,null,null,null,null);
        while(cursor.moveToNext()){
            list.add(new Product(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                    cursor.getInt(4),cursor.getString(6),cursor.getInt(7),cursor.getInt(8),cursor.getString(5)));
        }
        return list;
    }
    public List<Product> getTopViewedProducts(){
        List<Product> list = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.TopViewedProducts_id,database.TopViewedProducts_name,database.TopViewedProducts_image,
                database.TopViewedProducts_description,database.TopViewedProducts_price,database.TopViewedProducts_category_name,
                database.TopViewedProducts_avgRate,database.TopViewedProducts_offer_price,database.TopViewedProducts_is_offer};
        Cursor cursor = sqlitedatabase.query(database.TopViewedProducts,columns,null,null,null,null,null);
        while(cursor.moveToNext()){
            list.add(new Product(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                    cursor.getInt(4),cursor.getString(6),cursor.getInt(7),cursor.getInt(8),cursor.getString(5)));
        }
        return list;
    }
    public ProfileObject getProfile(){
        ProfileObject profile = new ProfileObject();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.Profile_token,database.Profile_product_notification_flag,database.Profile_offer_notification_flag,database.Profile_mobile_number,database.Profile_id};
        Cursor cursor = sqlitedatabase.query(database.Profile,columns,null,null,null,null,null);
        while (cursor.moveToNext()){
            profile.setToken(cursor.getString(0));
            profile.setProduct_notification_flag(cursor.getInt(1));
            profile.setOffer_notification_flag(cursor.getInt(2));
            profile.setMobile_number(cursor.getString(3));
            profile.setId(cursor.getInt(4));
        }
        return profile;
    }
    public List<Notification> getNotifications(){
        List<Notification> listOfNoties = new ArrayList<>();
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] columns = {database.Notifications_text,database.Notifications_date};
        Cursor cursor = sqlitedatabase.query(database.Notifications,columns,null,null,null,null,null);
        while (cursor.moveToNext()){
            listOfNoties.add(new Notification(cursor.getString(0),cursor.getString(1)));
        }
        return listOfNoties;
    }
    //Clear Tables------------------------------------------------------------------------------------------
    public int clearConfigs(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.Configs,null,null);
        return result;
    }
    public int clearConfigurations(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.Configurations,null,null);
        return result;
    }
    public int clearSlider(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.Slider,null,null);
        return result;
    }
    public int clearCategories(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.Categories,null,null);
        return result;
    }
    public int clearProducts(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.Products,null,null);
        return result;
    }
    public int clearNormalOffers(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.NormalOffers,null,null);
        return result;
    }
    public int clearSpecialOffers(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.SpecialOffers,null,null);
        return result;
    }
    public int clearSpecialOfferProducts(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.SpecialOfferProducts,null,null);
        return result;
    }
    public int clearSpecialOfferImages(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.SpecialOfferImages,null,null);
        return result;
    }
    public int clearTopRatedProducts(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.TopRatedProducts,null,null);
        return result;
    }
    public int clearTopSearchedProducts(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.TopSearchedProducts,null,null);
        return result;
    }
    public int clearTopViewedProducts(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.TopViewedProducts,null,null);
        return result;
    }
    public int clearProfile(){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        int result = sqlitedatabase.delete(database.Profile,null,null);
        return result;
    }
    //Update Rows---------------------------------------------------------------------------------------
    public int updateProfile(int offerFlag,int productFlag,String mobileNumber){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.Profile_offer_notification_flag,offerFlag);
        contentvalues.put(database.Profile_product_notification_flag,productFlag);
        contentvalues.put(database.Profile_mobile_number,mobileNumber);
        int result = sqlitedatabase.update(database.Profile,contentvalues,null,null);
        return result;
    }
    public int updateProduct(int id,String name,String image,String description,int price,String avgRate,int offer_price,int is_offer,String category_name,int available){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.Products_id,id);
        contentvalues.put(database.Products_name,name);
        contentvalues.put(database.Products_image,image);
        contentvalues.put(database.Products_description,description);
        contentvalues.put(database.Products_price,price);
        contentvalues.put(database.Products_avgRate,avgRate);
        contentvalues.put(database.Products_offer_price,offer_price);
        contentvalues.put(database.Products_is_offer,is_offer);
        contentvalues.put(database.Products_category_name,category_name);
        contentvalues.put(database.Products_available,available);
        String[] whereArgs = {String.valueOf(id)};
        int result = sqlitedatabase.update(database.Products,contentvalues,database.Products_id+"=?",whereArgs);
        return result;
    }
    public int updateNormalOffer(int id,String name,String image,String description,int price,String category_name,String avgRate,
                                 int offer_price,int is_offer,int available){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.NormalOffers_id,id);
        contentvalues.put(database.NormalOffers_name,name);
        contentvalues.put(database.NormalOffers_image,image);
        contentvalues.put(database.NormalOffers_description,description);
        contentvalues.put(database.NormalOffers_price,price);
        contentvalues.put(database.NormalOffers_avgRate,avgRate);
        contentvalues.put(database.NormalOffers_offer_price,offer_price);
        contentvalues.put(database.NormalOffers_is_offer,is_offer);
        contentvalues.put(database.NormalOffers_category_name,category_name);
        contentvalues.put(database.NormalOffers_available,available);
        String[] whereArgs = {String.valueOf(id)};
        int result = sqlitedatabase.update(database.NormalOffers,contentvalues,database.NormalOffers_id+"=?",whereArgs);
        return result;
    }
    public int updateProductsIsRated(int product_id,int value){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.Products_is_rated,value);
        String[] whereArgs = {String.valueOf(product_id)};
        int result = sqlitedatabase.update(database.Products,contentvalues,database.Products_id+"=?",whereArgs);
        return result;
    }
    public int updateNormalOfferIsRated(int offer_id,int value){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.NormalOffers_is_rated,value);
        String[] whereArgs = {String.valueOf(offer_id)};
        int result = sqlitedatabase.update(database.NormalOffers,contentvalues,database.NormalOffers_id+"=?",whereArgs);
        return result;
    }
    public int updateSpecialOfferRemain(int offer_id,long leftTime){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        ContentValues contentvalues=new ContentValues();
        contentvalues.put(database.SpecialOffers_offer_remain,String.valueOf(leftTime)+".0");
        String[] whereArgs = {String.valueOf(offer_id)};
        int result = sqlitedatabase.update(database.SpecialOffers,contentvalues,database.SpecialOffers_id+"=?",whereArgs);
        return result;
    }
    //Delete Rows
    public int deleteProductImages(int product_id){
        SQLiteDatabase sqlitedatabase=database.getWritableDatabase();
        String[] whereArgs = {String.valueOf(product_id)};
        int result = sqlitedatabase.delete(database.ProductImages,database.ProductImages_product_id+"=?",whereArgs);
        return result;
    }
    static class DB extends SQLiteOpenHelper{
        private Context context;
        private static final String database_name = "InitialDB.db";
        private static final int database_version = 19;
        private static final String Configs = "Configs";
        private static final String Configurations = "Configurations";
        private static final String Slider = "Slider";
        private static final String Categories = "Categories";
        private static final String Products = "Products";
        private static final String ProductImages = "ProductImages";
        private static final String NormalOffers = "NormalOffers";
        private static final String SpecialOffers = "SpecialOffers";
        private static final String SpecialOfferProducts = "SpecialOfferProducts";
        private static final String SpecialOfferImages = "SpecialOfferImages";
        private static final String TopSearchedProducts = "TopSearchedProducts";
        private static final String TopRatedProducts = "TopRatedProducts";
        private static final String TopViewedProducts = "TopViewedProducts";
        private static final String Profile = "Profile";
        private static final String Notifications = "Notifications";
        //----------------------------------------------------------------------------------------------------------------
        //Configs Columns
        private static final String Configs_id = "id";
        private static final String Configs_name = "name";
        private static final String Configs_unique_name = "unique_name";
        //Configurations Columns
        private static final String Configurations_id = "id";
        private static final String Configurations_config_id = "config_id";
        private static final String Configurations_name = "name";
        private static final String Configurations_value = "value";
        //Slider Columns
        private static final String Slider_id = "id";
        private static final String Slider_title = "title";
        private static final String Slider_image = "image";
        private static final String Slider_target = "target";
        private static final String Slider_url = "url";
        //Categories Columns
        private static final String Categories_id = "id";
        private static final String Categories_name = "name";
        private static final String Categories_image = "image";
        private static final String Categories_icon_image = "icon_image";
        private static final String Categories_parent_id = "parent_id";
        //Products Columns
        private static final String Products_id = "id";
        private static final String Products_name = "name";
        private static final String Products_image = "image";
        private static final String Products_description = "description";
        private static final String Products_price = "price";
        private static final String Products_avgRate = "avgRate";
        private static final String Products_offer_price = "offer_price";
        private static final String Products_is_offer = "is_offer";
        private static final String Products_category_name = "category_name";
        private static final String Products_available = "available";
        private static final String Products_is_rated = "is_rated";
        private static final String ProductImages_id = "id";
        private static final String ProductImages_image = "image";
        private static final String ProductImages_product_id = "product_id";
        //Normal Offers Columns
        private static final String NormalOffers_id = "id";
        private static final String NormalOffers_name = "name";
        private static final String NormalOffers_image = "image";
        private static final String NormalOffers_description = "description";
        private static final String NormalOffers_price = "price";
        private static final String NormalOffers_category_name = "category_name";
        private static final String NormalOffers_avgRate = "avgRate";
        private static final String NormalOffers_offer_price = "offer_price";
        private static final String NormalOffers_is_offer = "is_offer";
        private static final String NormalOffers_available = "available";
        private static final String NormalOffers_is_rated = "is_rated";
        private static final String NormalOffers_offer_remain = "offer_remain";
        //Special Offers Columns
        private static final String SpecialOffers_id = "id";
        private static final String SpecialOffers_information = "information";
        private static final String SpecialOffers_image_url = "image_url";
        private static final String SpecialOffers_start_date = "start_date";
        private static final String SpecialOffers_end_date = "end_date";
        private static final String SpecialOffers_offer_price = "offer_price";
        private static final String SpecialOffers_price = "price";
        private static final String SpecialOffers_offer_remain = "offer_remain";
        //Special Offer Products
        private static final String SpecialOfferProducts_id = "id";
        private static final String SpecialOfferProducts_name = "name";
        private static final String SpecialOfferProducts_image = "image";
        private static final String SpecialOfferProducts_description = "description";
        private static final String SpecialOfferProducts_price = "price";
        private static final String SpecialOfferProducts_category_name = "category_name";
        private static final String SpecialOfferProducts_is_offer = "is_offer";
        private static final String SpecialOfferProducts_offer_price = "offer_price";
        private static final String SpecialOfferProducts_avgRate = "avgRate";
        private static final String SpecialOfferProducts_offer_id = "offer_id";
        //Special Offer Images
        private static final String SpecialOfferImages_id = "id";
        private static final String SpecialOfferImages_offer_id = "offer_id";
        private static final String SpecialOfferImages_image_url = "image_url";
        //Top Searched Products Columns
        private static final String TopSearchedProducts_id = "id";
        private static final String TopSearchedProducts_name = "name";
        private static final String TopSearchedProducts_image = "image";
        private static final String TopSearchedProducts_description = "description";
        private static final String TopSearchedProducts_price = "price";
        private static final String TopSearchedProducts_category_name = "category_name";
        private static final String TopSearchedProducts_avgRate = "avgRate";
        private static final String TopSearchedProducts_offer_price = "offer_price";
        private static final String TopSearchedProducts_is_offer = "is_offer";
        //Top Rated Products Columns
        private static final String TopRatedProducts_id = "id";
        private static final String TopRatedProducts_name = "name";
        private static final String TopRatedProducts_image = "image";
        private static final String TopRatedProducts_description = "description";
        private static final String TopRatedProducts_price = "price";
        private static final String TopRatedProducts_category_name = "category_name";
        private static final String TopRatedProducts_avgRate = "avgRate";
        private static final String TopRatedProducts_offer_price = "offer_price";
        private static final String TopRatedProducts_is_offer = "is_offer";
        //Top Viewed Products Columns
        private static final String TopViewedProducts_id = "id";
        private static final String TopViewedProducts_name = "name";
        private static final String TopViewedProducts_image = "image";
        private static final String TopViewedProducts_description = "description";
        private static final String TopViewedProducts_price = "price";
        private static final String TopViewedProducts_category_name = "category_name";
        private static final String TopViewedProducts_avgRate = "avgRate";
        private static final String TopViewedProducts_offer_price = "offer_price";
        private static final String TopViewedProducts_is_offer = "is_offer";
        //Profile Columns
        private static final String Profile_token = "token";
        private static final String Profile_product_notification_flag = "product_notification_flag";
        private static final String Profile_offer_notification_flag = "offer_notification_flag";
        private static final String Profile_mobile_number = "mobile_number";
        private static final String Profile_id = "id";
        //Notifications Columns
        private static final String Notifications_text = "text";
        private static final String Notifications_date = "date";
        //----------------------------------------------------------------------------------------------------------------
        private static final String CREATE_CONFIGS_TABLE = "CREATE TABLE "+Configs+"("+Configs_id+" INTEGER, "+
                Configs_name+" VARCHAR(50), "+Configs_unique_name+" VARCHAR(50));";

        private static final String CREATE_CONFIGURATIONS_TABLE = "CREATE TABLE "+Configurations+"("+Configurations_id+" INTEGER, "+
                Configurations_config_id+" INTEGER, "+Configurations_name+" VARCHAR(50), "+Configurations_value+" VARCHAR(50));";

        private static final String CREATE_SLIDER_TABLE = "CREATE TABLE "+Slider+"("+Slider_id+" INTEGER, "+Slider_title+" VARCHAR(50), "+
                Slider_image+" VARCHAR(100), "+Slider_target+" INTEGER, "+Slider_url+" VARCHAR(100));";

        private static final String CREATE_CATEGORIES_TABLE = "CREATE TABLE "+Categories+"("+Categories_id+" INTEGER, "+
                Categories_name+" VARCHAR(50), "+Categories_image+" VARCHAR(100), "+Categories_icon_image+" VARCHAR(100), "+
                Categories_parent_id+" INTEGER);";

        private static final String CREATE_PRODUCTS_TABLE = "CREATE TABLE "+Products+"("+Products_id+" INTEGER, "+Products_name+" VARCHAR(50), "+
                Products_image+" VARCHAR(100), "+Products_description+" TEXT, "+Products_price+" INTEGER, "+Products_avgRate+" VARCHAR(50), "+
                Products_offer_price+" INTEGER, "+Products_is_offer+" INTEGER, "+Products_category_name+" VARCHAR(50), "+Products_available+" INTEGER, "+
                Products_is_rated+" INTEGER);";

        private static final String CREATE_PRODUCT_IMAGES_TABLE = "CREATE TABLE "+ProductImages+"("+ProductImages_id+" INTEGER, "+
                ProductImages_image+" VARCHAR(100), "+ProductImages_product_id+" INTEGER);";

        private static final String CREATE_NORMAL_OFFERS_TABLE = "CREATE TABLE "+NormalOffers+"("+NormalOffers_id+" INTEGER, "+
                NormalOffers_name+" VARCHAR(50), "+NormalOffers_image+" VARCHAR(100), "+NormalOffers_description+" TEXT, "+
                NormalOffers_price+" INTEGER, "+NormalOffers_category_name+" VARCHAR(50), "+NormalOffers_avgRate+" VARCHAR(50), "+
                NormalOffers_offer_price+" INTEGER, "+NormalOffers_is_offer+" INTEGER, "+NormalOffers_available+" INTEGER, "+
                NormalOffers_is_rated+" INTEGER, "+NormalOffers_offer_remain+" VARCHAR(50));";

        private static final String CREATE_SPECIAL_OFFERS_TABLE = "CREATE TABLE "+SpecialOffers+"("+SpecialOffers_id+" INTEGER, "+
                SpecialOffers_information+" TEXT, "+SpecialOffers_image_url+" VARCHAR(100), "+SpecialOffers_start_date+" VARCHAR(50), "+
                SpecialOffers_end_date+" VARCHAR(50), "+SpecialOffers_offer_price+" INTEGER, "+SpecialOffers_price+" INTEGER, "+
                SpecialOffers_offer_remain+" VARCHAR(50));";

        private static final String CREATE_SPECIAL_OFFER_PRODUCTS_TABLE ="CREATE TABLE "+SpecialOfferProducts+"("+SpecialOfferProducts_id+" INTEGER, "+
                SpecialOfferProducts_name+" VARCHAR(50), "+SpecialOfferProducts_image+" VARCHAR(100), "+SpecialOfferProducts_description+" TEXT, "+
                SpecialOfferProducts_price+" INTEGER, "+SpecialOfferProducts_category_name+" VARCHAR(50), "+SpecialOfferProducts_is_offer+" INTEGER, "+
                SpecialOfferProducts_offer_price+" INTEGER, "+SpecialOfferProducts_avgRate+" VARCHAR(50), "+SpecialOfferProducts_offer_id+" INTEGER);";

        private static final String CREATE_SPECIAL_OFFER_IMAGES_TABLE = "CREATE TABLE "+SpecialOfferImages+"("+SpecialOfferImages_id+" INTEGER, "+
                SpecialOfferImages_offer_id+" INTEGER, "+SpecialOfferImages_image_url+" VARCHAR(100));";

        private static final String CREATE_TOP_SEARCHED_PRODUCTS_TABLE = "CREATE TABLE "+TopSearchedProducts+"("+TopSearchedProducts_id+" INTEGER, "+
                TopSearchedProducts_name+" VARCHAR(50), "+TopSearchedProducts_image+" VARCHAR(100), "+TopSearchedProducts_description+" TEXT, "+
                TopSearchedProducts_price+" INTEGER, "+TopSearchedProducts_category_name+" VARCHAR(50), "+TopSearchedProducts_avgRate+" VARCHAR(50), "+
                TopSearchedProducts_offer_price+" INTEGER, "+TopSearchedProducts_is_offer+" INTEGER);";

        private static final String CREATE_TOP_RATED_PRODUCTS_TABLE = "CREATE TABLE "+TopRatedProducts+"("+TopRatedProducts_id+" INTEGER, "+
                TopRatedProducts_name+" VARCHAR(50), "+TopRatedProducts_image+" VARCHAR(100), "+TopRatedProducts_description+" TEXT, "+
                TopRatedProducts_price+" INTEGER, "+TopRatedProducts_category_name+" VARCHAR(50), "+TopRatedProducts_avgRate+" VARCHAR(50), "+
                TopRatedProducts_offer_price+" INTEGER, "+TopRatedProducts_is_offer+" INTEGER);";

        private static final String CREATE_TOP_VIEWED_PRODUCTS_TABLE = "CREATE TABLE "+TopViewedProducts+"("+TopViewedProducts_id+" INTEGER, "+
                TopViewedProducts_name+" VARCHAR(50), "+TopViewedProducts_image+" VARCHAR(100), "+TopViewedProducts_description+" TEXT, "+
                TopViewedProducts_price+" INTEGER, "+TopViewedProducts_category_name+" VARCHAR(50), "+TopViewedProducts_avgRate+" VARCHAR(50), "+
                TopViewedProducts_offer_price+" INTEGER, "+TopViewedProducts_is_offer+" INTEGER);";

        private static final String CREATE_PROFILE_TABLE = "CREATE TABLE "+Profile+"("+Profile_token+" TEXT, "+Profile_product_notification_flag+" INTEGER, "+
                Profile_offer_notification_flag+" INTEGER, "+Profile_mobile_number+" VARCHAR(50), "+Profile_id+" INTEGER);";

        private static final String CREATE_NOTIFICATIONS_TABLE = "CREATE TABLE "+Notifications+"("+Notifications_text+" TEXT, "+Notifications_date+" VARCHAR(50));";


        //----------------------------------------------------------------------------------------------------------------
        private static final String DROP_CONFIGS_TABLE = "DROP TABLE IF EXISTS "+Configs;
        private static final String DROP_CONFIGURATIONS_TABLE = "DROP TABLE IF EXISTS "+Configurations;
        private static final String DROP_SLIDER_TABLE = "DROP TABLE IF EXISTS "+Slider;
        private static final String DROP_CATEGORIES_TABLE = "DROP TABLE IF EXISTS "+Categories;
        private static final String DROP_PRODUCTS_TABLE = "DROP TABLE IF EXISTS "+Products;
        private static final String DROP_PRODUCT_IMAGES_TABLE = "DROP TABLE IF EXISTS "+ProductImages;
        private static final String DROP_NORMAL_OFFERS_TABLE = "DROP TABLE IF EXISTS "+NormalOffers;
        private static final String DROP_SPECIAL_OFFERS_TABLE = "DROP TABLE IF EXISTS "+SpecialOffers;
        private static final String DROP_SPECIAL_OFFER_PRODUCTS_TABLE = "DROP TABLE IF EXISTS "+SpecialOfferProducts;
        private static final String DROP_SPECIAL_OFFER_IMAGES_TABLE = "DROP TABLE IF EXISTS "+SpecialOfferImages;
        private static final String DROP_TOP_SEARCHED_PRODUCTS_TABLE = "DROP TABLE IF EXISTS "+TopSearchedProducts;
        private static final String DROP_TOP_RATED_PRODUCTS_TABLE ="DROP TABLE IF EXISTS "+TopRatedProducts;
        private static final String DROP_TOP_VIEWED_PRODUCTS_TABLE = "DROP TABLE IF EXISTS "+TopViewedProducts;
        private static final String DROP_PROFILE_TABLE = "DROP TABLE IF EXISTS "+Profile;
        private static final String DROP_NOTIFICTAIONS_TABLE = "DROP TABLE IF EXISTS "+Notifications;
        //----------------------------------------------------------------------------------------------------------------
        public DB(Context context) {
            super(context,database_name, null, database_version);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            try {
                sqLiteDatabase.execSQL(CREATE_CONFIGS_TABLE);
                sqLiteDatabase.execSQL(CREATE_CONFIGURATIONS_TABLE);
                sqLiteDatabase.execSQL(CREATE_SLIDER_TABLE);
                sqLiteDatabase.execSQL(CREATE_CATEGORIES_TABLE);
                sqLiteDatabase.execSQL(CREATE_PRODUCTS_TABLE);
                sqLiteDatabase.execSQL(CREATE_PRODUCT_IMAGES_TABLE);
                sqLiteDatabase.execSQL(CREATE_NORMAL_OFFERS_TABLE);
                sqLiteDatabase.execSQL(CREATE_SPECIAL_OFFERS_TABLE);
                sqLiteDatabase.execSQL(CREATE_SPECIAL_OFFER_PRODUCTS_TABLE);
                sqLiteDatabase.execSQL(CREATE_SPECIAL_OFFER_IMAGES_TABLE);
                sqLiteDatabase.execSQL(CREATE_TOP_SEARCHED_PRODUCTS_TABLE);
                sqLiteDatabase.execSQL(CREATE_TOP_RATED_PRODUCTS_TABLE);
                sqLiteDatabase.execSQL(CREATE_TOP_VIEWED_PRODUCTS_TABLE);
                sqLiteDatabase.execSQL(CREATE_PROFILE_TABLE);
                sqLiteDatabase.execSQL(CREATE_NOTIFICATIONS_TABLE);
            }catch (SQLException e){
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
           try {
               sqLiteDatabase.execSQL(DROP_CONFIGS_TABLE);
               sqLiteDatabase.execSQL(DROP_CONFIGURATIONS_TABLE);
               sqLiteDatabase.execSQL(DROP_SLIDER_TABLE);
               sqLiteDatabase.execSQL(DROP_CATEGORIES_TABLE);
               sqLiteDatabase.execSQL(DROP_PRODUCTS_TABLE);
               sqLiteDatabase.execSQL(DROP_PRODUCT_IMAGES_TABLE);
               sqLiteDatabase.execSQL(DROP_NORMAL_OFFERS_TABLE);
               sqLiteDatabase.execSQL(DROP_SPECIAL_OFFERS_TABLE);
               sqLiteDatabase.execSQL(DROP_SPECIAL_OFFER_PRODUCTS_TABLE);
               sqLiteDatabase.execSQL(DROP_SPECIAL_OFFER_IMAGES_TABLE);
               sqLiteDatabase.execSQL(DROP_TOP_SEARCHED_PRODUCTS_TABLE);
               sqLiteDatabase.execSQL(DROP_TOP_RATED_PRODUCTS_TABLE);
               sqLiteDatabase.execSQL(DROP_TOP_VIEWED_PRODUCTS_TABLE);
               sqLiteDatabase.execSQL(DROP_PROFILE_TABLE);
               sqLiteDatabase.execSQL(DROP_NOTIFICTAIONS_TABLE);
               onCreate(sqLiteDatabase);
           }catch (SQLException e){
               e.printStackTrace();
           }
        }
    }
}
