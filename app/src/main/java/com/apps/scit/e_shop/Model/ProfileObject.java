package com.apps.scit.e_shop.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 7/22/2018.
 */

public class ProfileObject {
    @SerializedName("token") private String token = "";
    @SerializedName("product_notification_flag") private int product_notification_flag=0;
    @SerializedName("offer_notification_flag") private int offer_notification_flag=0;
    @SerializedName("mobile_number") private String mobile_number="";
    @SerializedName("id") private int id = 0;
    @SerializedName("fcm_token") private String fcm_token = "";

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getProduct_notification_flag() {
        return product_notification_flag;
    }

    public void setProduct_notification_flag(int product_notification_flag) {
        this.product_notification_flag = product_notification_flag;
    }

    public int getOffer_notification_flag() {
        return offer_notification_flag;
    }

    public void setOffer_notification_flag(int offer_notification_flag) {
        this.offer_notification_flag = offer_notification_flag;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }
}
