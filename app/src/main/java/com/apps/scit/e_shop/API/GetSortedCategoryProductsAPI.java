package com.apps.scit.e_shop.API;

import com.apps.scit.e_shop.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 8/1/2018.
 */

public interface GetSortedCategoryProductsAPI {
    @GET("api/v1/category/{catid}/sort/{sort}")
    Call<BaseResponse> getProducts(@Path("catid")String cat_id,
                                   @Path("sort")String sort,
                                   @Query("page")int page);
}
