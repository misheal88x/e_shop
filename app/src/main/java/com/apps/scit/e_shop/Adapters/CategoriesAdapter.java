package com.apps.scit.e_shop.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.scit.e_shop.Model.Category;
import com.apps.scit.e_shop.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyViewHolder>{
    private Context context;
    private List<Category> listOfCategories;
    private OnItemClickListenter mListener;
public interface OnItemClickListenter{
    void onItemClick(int position);
}
public void setOnItemClickListener(OnItemClickListenter listener){
    mListener = listener;
}
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView categoryName;
        public ImageView categoryImage;
        public RelativeLayout relativeLayout;
        public MyViewHolder(View view, final OnItemClickListenter listenter){
            super(view);
            categoryName = view.findViewById(R.id.category_name);
            categoryImage = view.findViewById(R.id.category_image);
            relativeLayout = view.findViewById(R.id.category_card_relativelayout);
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listenter!=null){
                        int position = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listenter.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
    public CategoriesAdapter(Context context, List<Category> listOfCategories){
        this.context = context;
        this.listOfCategories = listOfCategories;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_card,parent,false);
        return new MyViewHolder(itemView,mListener);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Category category = listOfCategories.get(position);
        holder.categoryName.setText(category.getName());
        if (category.getImage()!=null){
            GlideApp.with(context).load(category.getImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.offline).centerCrop().into(holder.categoryImage);
            //Picasso.with(context).load(category.getImage()).placeholder(R.drawable.offline).into(holder.categoryImage);
        }
        /*
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CategoryProducts.class);
                context.startActivity(intent);
            }
        });
        **/
    }
    @Override
    public int getItemCount() {
        return listOfCategories.size();
    }
}
