package com.apps.scit.e_shop.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 7/10/2018.
 */

public class ConfigMainObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("unqiue_name") private String unqiue_name = "";
    @SerializedName("configurations") private List<ConfigObject> configurations = new ArrayList<>();

    public ConfigMainObject(int id, String name, String unique_name) {
        this.id = id;
        this.name = name;
        this.unqiue_name = unique_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnqiue_name() {
        return unqiue_name;
    }

    public void setUnique_name(String unique_name) {
        this.unqiue_name = unique_name;
    }

    public List<ConfigObject> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(List<ConfigObject> configurations) {
        this.configurations = configurations;
    }
}
