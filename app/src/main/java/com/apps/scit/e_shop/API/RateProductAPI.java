package com.apps.scit.e_shop.API;

import com.apps.scit.e_shop.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Misheal on 7/29/2018.
 */

public interface RateProductAPI {
    @FormUrlEncoded
    @POST("api/v1/product/rate")
    Call<BaseResponse> rate(@Field("value")String value,
                            @Field("comment")String comment,
                            @Field("product_id")int product_id);
}
