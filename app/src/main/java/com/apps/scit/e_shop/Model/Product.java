package com.apps.scit.e_shop.Model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Product {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("full_image_url") private String full_image_url = "";
    @SerializedName("description") private String description = "";
    @SerializedName("price") private int price = 0 ;
    @SerializedName("avgRate") private String avgRate = "";
    @SerializedName("offer_price") private int offer_price = 0;
    @SerializedName("is_offer") private int is_offer = 0;
    @SerializedName("category_name") private String category_name = "";
    @SerializedName("created_time") private String created_time ="";
    //@SerializedName("shown") private int shown = 0;
    @SerializedName("available") private int available = 0;
    @SerializedName("view_count") private int view_count = 0;
    @SerializedName("search_count") private int search_count = 0;
    //@SerializedName("rates") private List<ProductRate> rates = new ArrayList<>();
    @SerializedName("is_rated") private int is_rated = 0;
    @SerializedName("images") private List<String> images = new ArrayList<>();
    @SerializedName("related_products") private List<Product> related_products = new ArrayList<>();
    //@SerializedName("is_user_rated") private int is_user_rated = 0;
    @SerializedName("offer_remain") private int offer_remain = 0;
    //@SerializedName("offer_id") private int offer_id = 0;
    public Product(){}
    public Product(String name, String description, int price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public Product(String name, int price, String avgRate, int offer_price) {
        this.name = name;
        this.price = price;
        this.avgRate = avgRate;
        this.offer_price = offer_price;
    }

    public Product(int id, String name, String full_image_url
            , String description, int price
            , String avgRate, int offer_price
            , int is_offer, String category_name) {
        this.id = id;
        this.name = name;
        this.full_image_url = full_image_url;
        this.description = description;
        this.price = price;
        this.avgRate = avgRate;
        this.offer_price = offer_price;
        this.is_offer = is_offer;
        this.category_name = category_name;
    }
    public Product(int id, String name, String full_image_url
            , String description, int price
            , String avgRate, int offer_price
            , int is_offer, String category_name,int offer_remain) {
        this.id = id;
        this.name = name;
        this.full_image_url = full_image_url;
        this.description = description;
        this.price = price;
        this.avgRate = avgRate;
        this.offer_price = offer_price;
        this.is_offer = is_offer;
        this.category_name = category_name;
        this.offer_remain = offer_remain;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribtion() {
        return description;
    }

    public void setDescribtion(String describtion) {
        this.description = describtion;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }


    public String getImage() {
        return full_image_url;
    }

    public void setImage(String full_image_url) {
        this.full_image_url = full_image_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvgRate() {
        return avgRate;
    }

    public void setAvgRate(String avgRate) {
        this.avgRate = avgRate;
    }

    public int getOffer_price() {
        return offer_price;
    }

    public void setOffer_price(int offer_price) {
        this.offer_price = offer_price;
    }

    public int getIs_offer() {
        return is_offer;
    }

    public void setIs_offer(int is_offer) {
        this.is_offer = is_offer;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    /*
    public int getShown() {
        return shown;
    }

    public void setShown(int shown) {
        this.shown = shown;
    }
**/
    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getView_count() {
        return view_count;
    }

    public void setView_count(int view_count) {
        this.view_count = view_count;
    }


    public int getSearch_count() {
        return search_count;
    }

    public void setSearch_count(int search_count) {
        this.search_count = search_count;
    }
/*
    public List<ProductRate> getRates() {
        return rates;
    }

    public void setRates(List<ProductRate> rates) {
        this.rates = rates;
    }
**/
    public int getIs_rated() {
        return is_rated;
    }

    public void setIs_rated(int is_rated) {
        this.is_rated = is_rated;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<Product> getRelated_products() {
        return related_products;
    }

    public void setRelated_products(List<Product> related_products) {
        this.related_products = related_products;
    }


    /*
    public int getIs_user_rated() {
        return is_user_rated;
    }

    public void setIs_user_rated(int is_user_rated) {
        this.is_user_rated = is_user_rated;
    }

**/
    public int getOffer_remain() {
        return offer_remain;
    }

    public void setOffer_remain(int offer_remain) {
        this.offer_remain = offer_remain;
    }

    public String getFull_image_url() {
        return full_image_url;
    }

    public void setFull_image_url(String full_image_url) {
        this.full_image_url = full_image_url;
    }
}
