package com.apps.scit.e_shop.Activities;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.scit.e_shop.Adapters.OfferProductsAdapter;
import com.apps.scit.e_shop.Adapters.OffersAdapter;
import com.apps.scit.e_shop.Adapters.RelatedProductsAdapter;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.Model.Offer;
import com.apps.scit.e_shop.Model.Product;
import com.apps.scit.e_shop.R;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AllOffers extends AppCompatActivity {
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    Toolbar toolbar;
    TextView toolbarTitle;
    RecyclerView singleProductOffersRecycler;
    ViewPager offersViewPager;
    List<Offer> listOfOffers;
    List<Product> listOfSingleProductOffers;
    OffersAdapter offersAdapter;
    OfferProductsAdapter singleProductOffersAdapter;
    NestedScrollView scrollView;
    TextView specialOffersText,normalOffersText;
    LinearLayout allNormalOffersLinearLayout;
    Database database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_offers);
        definingIDs();
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getSupportActionBar().setTitle("");
        toolbarTitle.setText("العروض");
        //Setting up ScrollView
        scrollView.setFocusableInTouchMode(true);
        scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        getDataFromSQLite();
        RecyclerView.LayoutManager layoutManager2 = new GridLayoutManager(this,2);
        singleProductOffersRecycler.setLayoutManager(layoutManager2);
        offersAdapter = new OffersAdapter(this,listOfOffers);
        singleProductOffersAdapter = new OfferProductsAdapter(this,listOfSingleProductOffers);
        offersViewPager.setAdapter(offersAdapter);
        runAnimation(singleProductOffersRecycler,0,singleProductOffersAdapter);
        //singleProductOffersRecycler.setAdapter(singleProductOffersAdapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.all_offers_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            this.finish();
        }
        return true;
    }
    public void definingIDs(){
        //Toolbar
        toolbar = findViewById(R.id.all_offers_toolbar);
        toolbarTitle = toolbar.findViewById(R.id.basicToolbarTitle);
        //ScrollView
        scrollView = findViewById(R.id.all_offers_scroll_view);
        //TextView
        specialOffersText = findViewById(R.id.all_multiple_products_offers_text);
        normalOffersText = findViewById(R.id.all_single_product_offers_text);
        //LinearLayout
        allNormalOffersLinearLayout = findViewById(R.id.all_normal_offers_linearlayout);
        //Database
        database = new Database(AllOffers.this);
        //View Pager
        offersViewPager = findViewById(R.id.all_all_multiple_products_offers_viewpager);
        //RecyclerView
        singleProductOffersRecycler = findViewById(R.id.all_all_single_product_offers_recycler);
    }
    public void getDataFromSQLite(){

        listOfOffers = new ArrayList<>();
        listOfSingleProductOffers = new ArrayList<>();
        if (database.getSpecialOffers().size()==0&&database.getNormalOffers().size()==0){
            Toast.makeText(this, "لا يوجد عروض", Toast.LENGTH_SHORT).show();
            finish();
        }
        //Special Offers
        if (database.getSpecialOffers().size()>0){
            for (Offer o : database.getSpecialOffers()){
                listOfOffers.add(o);
            }
        }else {
            specialOffersText.setVisibility(View.GONE);
            offersViewPager.setVisibility(View.GONE);
        }
        //Normal Offers
        if (database.getNormalOffers().size()>0){
            for (Product p : database.getNormalOffers()){
                listOfSingleProductOffers.add(p);
            }
        }else {
            allNormalOffersLinearLayout.setVisibility(View.GONE);
            normalOffersText.setVisibility(View.GONE);
        }
    }
    public void runAnimation(RecyclerView recyclerView,int type,OfferProductsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
