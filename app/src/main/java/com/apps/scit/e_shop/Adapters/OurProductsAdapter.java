package com.apps.scit.e_shop.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.scit.e_shop.Activities.ProductDetails;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.Model.Product;
import com.apps.scit.e_shop.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Misheal on 7/17/2018.
 */

public class OurProductsAdapter extends RecyclerView.Adapter<OurProductsAdapter.MyViewHolder> {
    private Context context;
    private List<Product> listOfProducts;
    private Database database;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView productName;
        public TextView productPrice;
        public ImageView productImage;
        public RatingBar productRate;
        public RelativeLayout layout;
        public MyViewHolder(View view){
            super(view);
            productName = view.findViewById(R.id.our_product_name);
            productPrice = view.findViewById(R.id.our_product_price);
            productRate = view.findViewById(R.id.our_product_rate);
            productImage = view.findViewById(R.id.our_product_image);
            layout =view.findViewById(R.id.our_products_layout);
        }
    }
    public OurProductsAdapter(Context context, List<Product> listOfProducts){
        this.context = context;
        this.listOfProducts = listOfProducts;
        this.database = new Database(context);
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.our_products_card,parent,false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Product product = listOfProducts.get(position);
        holder.productName.setText(product.getName());
        holder.productPrice.setText(String.valueOf(product.getPrice()));
        try{
            holder.productRate.setRating(Float.parseFloat(product.getAvgRate()));
        }catch (Exception e){}
        if (product.getImage()!=null){
            GlideApp.with(context).load(product.getImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.offline).centerCrop().into(holder.productImage);
            //Picasso.with(context).load(product.getImage()).placeholder(R.drawable.offline).into(holder.productImage);
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!(isOnline()) && database.getSingleProduct(product.getId()).getId() == 0) {
                    Toast.makeText(context, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences sharedPreferences = context.getSharedPreferences("product", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("productType", "product");
                    editor.putInt("product_id", product.getId());
                    editor.commit();
                    Intent intent = new Intent(context, ProductDetails.class);
                    intent.putExtra("product_id",String.valueOf(product.getId()));
                    intent.putExtra("productType","product");
                    context.startActivity(intent);
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return listOfProducts.size();
    }

    public void addProducts(List<Product> list){
        for (Product p : list){
            listOfProducts.add(p);
        }
        notifyDataSetChanged();
    }
    public boolean isOnline(){
        ConnectivityManager conMgr =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null){
            return false;
        }else {
            return true;
        }
    }

}
