package com.apps.scit.e_shop.Extra;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.apps.scit.e_shop.API.UpdateProfileAPI;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.Model.BaseResponse;
import com.apps.scit.e_shop.R;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Misheal on 8/4/2018.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    public static final String TAG = "FCMTOKEN";
    public static final String ROLE = "FCMROLE";
    public static int profile=-1;
    public static String role="";
    Database database;
    private static  String BASE_URL;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        //Base url
        BASE_URL = getResources().getString(R.string.base_url);
        database = new Database(this);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.i("refreshed_token", "the token: " + refreshedToken);
        if (refreshedToken!=null){
            SharedPreferences sharedPreferences = getSharedPreferences("Profile", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (!refreshedToken.equals(sharedPreferences.getString("notificationsToken","")))
                sendRegistrationToServer(refreshedToken);
            editor.putString("notificationsToken",refreshedToken);
            editor.commit();
        }

    }

    private void sendRegistrationToServer(String token) {
        callUpdateProfileAPI(token);
    }

    private void callUpdateProfileAPI(String fcm_token){
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        String mobileNumber = database.getProfile().getMobile_number();
        String productFlag = booleanConverter(database.getProfile().getProduct_notification_flag());
        String offerFlag = booleanConverter(database.getProfile().getOffer_notification_flag());
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        UpdateProfileAPI updateAPI = retrofit.create(UpdateProfileAPI.class);
        Call<BaseResponse> call;

        if (mobileNumber.equals("")){
            call = updateAPI.update("0000000000",productFlag,offerFlag,fcm_token);
        }else {
            call = updateAPI.update(mobileNumber,productFlag,offerFlag,fcm_token);
        }
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
            }
        });
    }
    public String booleanConverter(int input){
        if (input==0){
            return "no";
        }else {
            return "yes";
        }
    }
}
