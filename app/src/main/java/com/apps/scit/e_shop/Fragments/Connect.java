package com.apps.scit.e_shop.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.apps.scit.e_shop.API.GetMessagesAPI;
import com.apps.scit.e_shop.API.SendMessageAPI;
import com.apps.scit.e_shop.Adapters.ConnectAdapter;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.Model.BaseResponse;
import com.apps.scit.e_shop.Model.Message;
import com.apps.scit.e_shop.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
public class Connect extends Fragment {
RecyclerView messagesRecycler;
List<Message> listOfMessages;
ConnectAdapter adapter;
ProgressBar loadMessages;
EditText messageText;
ImageButton sendMessage;
Database database;
private static  String BASE_URL;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.connect_fragment,container,false);
        //Defining IDs
        //Base url
        BASE_URL = getResources().getString(R.string.base_url);
        messagesRecycler = view.findViewById(R.id.messages_recycler);
        loadMessages = view.findViewById(R.id.connect_load_messages);
        messageText = view.findViewById(R.id.message_edittext);
        sendMessage = view.findViewById(R.id.send_message_btn);
        database = new Database(getContext());
        listOfMessages = new ArrayList<>();
        //CallGetMessagesAPI
        loadMessages.setVisibility(View.VISIBLE);
        callGetMessagesAPI();
        adapter = new ConnectAdapter(getContext(),listOfMessages);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        messagesRecycler.setLayoutManager(layoutManager);
        messagesRecycler.setAdapter(adapter);
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              if (messageText.getText().toString().equals("")){
                  Toast.makeText(getContext(), "لا يمكنك إرسال رسالة فارغة", Toast.LENGTH_SHORT).show();
              }else {
                  final String messageTxt = messageText.getText().toString();
                  String device_id = android.provider.Settings.Secure.getString(getContext().getContentResolver(),
                          android.provider.Settings.Secure.ANDROID_ID);
                  SharedPreferences sharedPreferences = getContext().getSharedPreferences("Profile", Context.MODE_PRIVATE);
                  final String token = sharedPreferences.getString("token","");
                  OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                      @Override
                      public okhttp3.Response intercept(Chain chain) throws IOException {
                          Request newRequest  = chain.request().newBuilder()
                                  .addHeader("Authorization", "Bearer " + token)
                                  .build();
                          return chain.proceed(newRequest);
                      }
                  }).build();
                  Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
                  SendMessageAPI api = retrofit.create(SendMessageAPI.class);
                  Call<BaseResponse> call = null;
                  call = api.sendMessage(device_id,messageTxt);
                  call.enqueue(new Callback<BaseResponse>() {
                      @Override
                      public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                          if (response.body().getCode()==200&response.body().getMessage_code()==111){
                              Toast.makeText(getContext(), "تم إرسال الرسالة بنجاح", Toast.LENGTH_SHORT).show();
                              messageText.setText("");
                              SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss",Locale.getDefault());
                              String myDate = format.format(new Date());
                              int id = database.getProfile().getId();
                              Message message = new Message();
                              message.setSender_id(id);
                              message.setReceiver_id(0);
                              message.setContent(messageTxt);
                              message.setMsg_time(myDate);
                              listOfMessages.add(message);
                              adapter.notifyDataSetChanged();

                          }else {
                              Toast.makeText(getContext(), "حدث خطأ ما يرجى إعادة المحاولة", Toast.LENGTH_SHORT).show();
                          }
                      }

                      @Override
                      public void onFailure(Call<BaseResponse> call, Throwable t) {
                          Toast.makeText(getContext(), "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
                      }
                  });
              }
            }
        });
        return view;
    }
    public void callGetMessagesAPI(){
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("Profile", Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetMessagesAPI api = retrofit.create(GetMessagesAPI.class);
        Call<BaseResponse> call = null;
        call = api.getMessages("android");
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    Message[] success = new Gson().fromJson(json,Message[].class);
                    if (success.length>0){
                        for (Message m : success){
                            listOfMessages.add(m);
                            adapter.notifyDataSetChanged();
                        }
                        loadMessages.setVisibility(View.GONE);
                    }else {
                        Toast.makeText(getContext(), "لا يوجد رسائل", Toast.LENGTH_SHORT).show();
                        loadMessages.setVisibility(View.GONE);
                    }

                }else {
                    Toast.makeText(getContext(), "فشل في الحصول على الرسائل حاول مجددا!", Toast.LENGTH_SHORT).show();
                    loadMessages.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(getContext(), "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
                loadMessages.setVisibility(View.GONE);
            }
        });
    }

    /*
    @Override
    public boolean onBackPressed() {
        return true;
    }
    **/
}
