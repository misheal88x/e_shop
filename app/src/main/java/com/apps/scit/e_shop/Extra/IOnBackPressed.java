package com.apps.scit.e_shop.Extra;

/**
 * Created by Misheal on 7/26/2018.
 */

public interface IOnBackPressed {
    boolean onBackPressed();
}
