package com.apps.scit.e_shop.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.apps.scit.e_shop.API.UpdateProfileAPI;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.Model.BaseResponse;
import com.apps.scit.e_shop.R;
import com.rm.rmswitch.RMSwitch;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Misheal on 6/25/2018.
 */

public class Settings extends Fragment {
    RMSwitch muteNewOffersSwitch,muteNewProductsSwitch;
    EditText mobileNumber;
    ImageButton setNumber;
    Boolean isOfferChecked = false
            ,isProductChecked = false;
    Database database;
    private ProgressDialog updateSettingsProgressDialog;
    private static String BASE_URL;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment,container,false);
        definingIDs(view);
        setSwithesColors();
        setDefaults();
        muteNewOffersSwitch.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                if (isOnline()){
                    if (mobileNumber.getText().toString().equals("")){
                        Toast.makeText(getContext(), "قم بإضافة رقمك أولا ثم أعد المحاولة", Toast.LENGTH_SHORT).show();
                        muteNewOffersSwitch.setChecked(!isChecked);
                    }else {
                        if (isChecked){
                            isOfferChecked = true;
                            if (isProductChecked){
                                callAPI("yes","yes",mobileNumber.getText().toString());
                                database.updateProfile(1,1,mobileNumber.getText().toString());
                            }else {
                                callAPI("yes","no",mobileNumber.getText().toString());
                                database.updateProfile(1,0,mobileNumber.getText().toString());
                            }
                        }else {
                            isOfferChecked = false;
                            if (isProductChecked){
                                callAPI("no","yes",mobileNumber.getText().toString());
                                database.updateProfile(0,1,mobileNumber.getText().toString());
                            }else {
                                callAPI("no","no",mobileNumber.getText().toString());
                                database.updateProfile(0,0,mobileNumber.getText().toString());
                            }
                        }
                    }
                }else {
                    Toast.makeText(getContext(), "لست متصلا بالانترنت, تأكد من اتصالك ثم أعد المحاولة", Toast.LENGTH_SHORT).show();
                    muteNewOffersSwitch.setChecked(!isChecked);
                }
            }
        });
        muteNewProductsSwitch.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                if (isOnline()){
                    if (mobileNumber.getText().toString().equals("")){
                        Toast.makeText(getContext(), "قم بإضافة رقمك أولا ثم أعد المحاولة", Toast.LENGTH_SHORT).show();
                        muteNewProductsSwitch.setChecked(!isChecked);
                    }else {
                        if (isChecked){
                            isProductChecked = true;
                            if (isOfferChecked){
                                callAPI("yes","yes",mobileNumber.getText().toString());
                                database.updateProfile(1,1,mobileNumber.getText().toString());
                            }else {
                                callAPI("no","yes",mobileNumber.getText().toString());
                                database.updateProfile(0,1,mobileNumber.getText().toString());
                            }
                        }else {
                            isProductChecked = false;
                            if (isOfferChecked){
                                callAPI("yes","no",mobileNumber.getText().toString());
                                database.updateProfile(1,0,mobileNumber.getText().toString());
                            }else {
                                callAPI("no","no",mobileNumber.getText().toString());
                                database.updateProfile(0,0,mobileNumber.getText().toString());
                            }
                        }
                    }
                }else {
                    Toast.makeText(getContext(), "لست متصلا بالانترنت, تأكد من اتصالك ثم أعد المحاولة", Toast.LENGTH_SHORT).show();
                    muteNewProductsSwitch.setChecked(!isChecked);
                }
            }
        });
        setNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isOnline()){
                    if (mobileNumber.getText().toString().equals("")){
                        Toast.makeText(getContext(), "لم تقم بتحديد رقم هاتف", Toast.LENGTH_SHORT).show();
                    }else {
                        Boolean result;
                        if (isOfferChecked&&isProductChecked) {
                            callAPI("yes", "yes", mobileNumber.getText().toString());
                            database.updateProfile(1,1,mobileNumber.getText().toString());
                                Toast.makeText(getContext(), "تمت إضافة الرقم", Toast.LENGTH_SHORT).show();

                        }else if (isOfferChecked==false&&isProductChecked){
                            callAPI("no", "yes", mobileNumber.getText().toString());
                            database.updateProfile(0,1,mobileNumber.getText().toString());

                                Toast.makeText(getContext(), "تمت إضافة الرقم", Toast.LENGTH_SHORT).show();

                        }else if (isOfferChecked&&isProductChecked==false){
                            callAPI("yes", "no", mobileNumber.getText().toString());
                            database.updateProfile(1,0,mobileNumber.getText().toString());

                                Toast.makeText(getContext(), "تمت إضافة الرقم", Toast.LENGTH_SHORT).show();

                        }else if (isOfferChecked==false&&isProductChecked==false){
                            callAPI("no", "no", mobileNumber.getText().toString());
                            database.updateProfile(0,0,mobileNumber.getText().toString());

                                Toast.makeText(getContext(), "تمت إضافة الرقم", Toast.LENGTH_SHORT).show();

                        }
                    }
                }else {
                    Toast.makeText(getContext(), "لست متصلا بالانترنت, تأكد من اتصالك ثم أعد المحاولة", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }


    public void definingIDs(View view){
        //Base url
        BASE_URL = getResources().getString(R.string.base_url);
        //Switchs
        muteNewOffersSwitch = view.findViewById(R.id.settings_mute_new_offers_notify_switch);
        muteNewProductsSwitch = view.findViewById(R.id.settings_mute_new_products_notify_switch);
        //Edittext
        mobileNumber = view.findViewById(R.id.settings_add_number_edittext);
        //ImageButton
        setNumber = view.findViewById(R.id.settings_add_number_check);
        //Database
        database = new Database(getContext());
        //Progress dialog
        updateSettingsProgressDialog = new ProgressDialog(getContext());
        updateSettingsProgressDialog.setMessage("جاري تحديث الإعدادات");
        updateSettingsProgressDialog.setCancelable(false);

    }
    public void setSwithesColors(){
        muteNewOffersSwitch.setSwitchBkgCheckedColor(getResources().getColor(R.color.green));
        muteNewOffersSwitch.setSwitchToggleCheckedColor(Color.WHITE);
        muteNewOffersSwitch.setSwitchToggleNotCheckedColor(Color.WHITE);

        muteNewProductsSwitch.setSwitchBkgCheckedColor(getResources().getColor(R.color.green));
        muteNewProductsSwitch.setSwitchToggleCheckedColor(Color.WHITE);
        muteNewProductsSwitch.setSwitchToggleNotCheckedColor(Color.WHITE);
    }
    public void setDefaults(){
        int offerFlag = database.getProfile().getOffer_notification_flag();
        int productFlag = database.getProfile().getProduct_notification_flag();
        String mobile = database.getProfile().getMobile_number();
        //OfferFlag
        if (offerFlag==0){
            isOfferChecked = false;
            muteNewOffersSwitch.setChecked(false);
        }else if (offerFlag==1){
            isOfferChecked = true;
            muteNewOffersSwitch.setChecked(true);
        }
        //ProductFlag
        if (productFlag==0){
            isProductChecked = false;
            muteNewProductsSwitch.setChecked(false);
        }else if (productFlag==1){
            isProductChecked = true;
            muteNewProductsSwitch.setChecked(true);
        }
        //Mobile Number
        if (!mobile.equals("")){
            mobileNumber.setText(mobile);
        }
    }
    public Boolean callAPI(String offerFlag,String productFlag,String mobileNumber){
        final Boolean[] result = {false};
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("Profile", Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        String fcm_token = sharedPreferences.getString("notificationsToken","");
        updateSettingsProgressDialog.show();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        UpdateProfileAPI api = retrofit.create(UpdateProfileAPI.class);
        Call<BaseResponse> call;
        call = api.update(mobileNumber,productFlag,offerFlag,fcm_token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                updateSettingsProgressDialog.cancel();
                if (response.body().getCode()==200&&response.body().getMessage_code()==114){
                    result[0] = true;
                    Toast.makeText(getContext(), "تم تحديث الإعدادات بنجاح", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getContext(), "حدث خطأ ما يرجى إعادة المحاولة", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                updateSettingsProgressDialog.cancel();
                Toast.makeText(getContext(), "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
            }
        });
        return result[0];
    }
    public boolean isOnline(){
        ConnectivityManager conMgr =  (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null){
            return false;
        }else {
            return true;
        }
    }

    /*
    @Override
    public boolean onBackPressed() {
        return true;
    }
    **/
}
