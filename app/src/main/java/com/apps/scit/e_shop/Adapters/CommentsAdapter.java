package com.apps.scit.e_shop.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.scit.e_shop.Model.ProductRate;
import com.apps.scit.e_shop.R;

import java.util.List;

/**
 * Created by Misheal on 6/23/2018.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {
    private Context context;
    private List<ProductRate> listOfComments;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView username,commenttext,numStars;
        public ImageView image;
        public MyViewHolder(View view){
            super(view);
            username = view.findViewById(R.id.comment_username);
            commenttext = view.findViewById(R.id.comment_text);
            numStars = view.findViewById(R.id.comment_stars_number);
            image = view.findViewById(R.id.comment_image);
        }
    }
    public CommentsAdapter(Context context, List<ProductRate> listOfComments){
        this.context = context;
        this.listOfComments = listOfComments;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_comment_row,parent,false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ProductRate comment = listOfComments.get(position);
        holder.username.setText("user "+comment.getVisitor_id());
        holder.commenttext.setText(comment.getComment());
        holder.numStars.setText(String.valueOf(comment.getValue())+".0");
    }
    @Override
    public int getItemCount() {
        return listOfComments.size();
    }
}
