package com.apps.scit.e_shop.Extra;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.apps.scit.e_shop.Activities.Notifications;
import com.apps.scit.e_shop.Activities.OfferDetails;
import com.apps.scit.e_shop.Activities.ProductDetails;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Misheal on 8/4/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    Database database = new Database(this);
    final int MY_NOTIFICATION_ID = 1;
    final String NOTIFICATION_CHANNEL_ID = "10001";
    NotificationManager notificationManager;
    //Notification myNotification;
    NotificationCompat.Builder builder;
    final Context context = this;
    public SharedPreferences sharedPreferences,specialOfferSharedPreferences;
    public SharedPreferences.Editor editor,specialOfferEditor;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Log.i("message_received", "onMessageReceived: "+"yes");
            Map<String, String> params = remoteMessage.getData();
            JSONObject jsonObject = new JSONObject(params);
            Log.i("JSON_OBJECT", "the : " + jsonObject.toString());
            sendNotification(jsonObject);
        }catch (Exception e){}
    }
    private void sendNotification(JSONObject jsonObject) {

        String content = "";
        String target_type = "";
        String target_id = "";
        sharedPreferences = getSharedPreferences("product", Context.MODE_PRIVATE);
        specialOfferSharedPreferences = getSharedPreferences("SpecialOffer",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        specialOfferEditor = specialOfferSharedPreferences.edit();
        try {
            content = jsonObject.getString("content");
            target_type = jsonObject.getString("type");
            target_id = jsonObject.getString("target");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("received_noti_data", "sendNotification: "+target_type+" "+target_id+" "+content);
        SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss", Locale.getDefault());
        String date = format.format(new Date());
        database.insertNotification(content,date);
        switch (target_type){
            //Admin
            case "-1" : {
                Intent intent = new Intent(context, Notifications.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        context,
                        1,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);
                showNotification(pendingIntent,content);
            }break;
            //New Product
            case "1" : {
                Log.i("get_here", "showNotification: "+"yes");

                Intent intent = new Intent(context, ProductDetails.class);
                intent.putExtra("product_id",target_id);
                intent.putExtra("productType","product");
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        context,
                        1,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                showNotification(pendingIntent,content);
            }break;
            //New Normal Offer
            case "2" : {
                Intent intent = new Intent(context, ProductDetails.class);
                intent.putExtra("product_id",target_id);
                intent.putExtra("productType","offer");
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        context,
                        1,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);
                showNotification(pendingIntent,content);
            }break;
            //New Special Offer
            case "3" : {
                Intent intent = new Intent(context, OfferDetails.class);
                intent.putExtra("offer_id",target_id);
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        context,
                        1,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);
                showNotification(pendingIntent,content);
            }break;
        }

    }

    private void showNotification(PendingIntent pendingIntent,String message){
        Log.i("get_here2", "showNotification: "+"yes");
        if (pendingIntent!=null){
            builder = new NotificationCompat.Builder(context)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message))
                    .setTicker("new notification")
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.logo)
                    .setVibrate(new long[]{500, 500, 500});
        }else {
            builder = new NotificationCompat.Builder(context)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message))
                    .setTicker("new notification")
                    .setWhen(System.currentTimeMillis())
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.logo)
                    .setVibrate(new long[]{500, 500, 500});
        }
        notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        //notificationManager.notify(MY_NOTIFICATION_ID, myNotification);
        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNAEL", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{500, 500, 500});
            assert notificationManager != null;
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        assert notificationManager != null;
        notificationManager.notify(MY_NOTIFICATION_ID, builder.build());
    }
}

