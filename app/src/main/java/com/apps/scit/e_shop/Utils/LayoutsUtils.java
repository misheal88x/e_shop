package com.apps.scit.e_shop.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;

/**
 * Created by Misheal on 6/21/2019.
 */

public class LayoutsUtils {
    //Force an activity to a specific direction
    public static void forceLayoutToADirection(Activity activity , int i){
        if (i == 0){
            if (activity.getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL){
                activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        }else {
            if (activity.getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_LTR){
                activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
        }
    }


}
