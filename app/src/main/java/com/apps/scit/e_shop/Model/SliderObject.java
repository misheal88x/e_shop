package com.apps.scit.e_shop.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 7/10/2018.
 */

public class SliderObject {
    //@SerializedName("id") private int id = 0;
    @SerializedName("title") private String title = "";
    @SerializedName("full_image_url") private String full_image_url = "";
    @SerializedName("target") private int target = 0;
    @SerializedName("url") private String url = "";

    public SliderObject(int id, String title, String full_image_url, int target, String url) {
        //this.id = id;
        this.title = title;
        this.full_image_url = full_image_url;
        this.target = target;
        this.url = url;
    }

    /*
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    **/

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return full_image_url;
    }

    public void setImage(String image) {
        this.full_image_url = image;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
