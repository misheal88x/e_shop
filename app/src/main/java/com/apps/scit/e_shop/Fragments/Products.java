package com.apps.scit.e_shop.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.apps.scit.e_shop.API.GetAllProductsAPI;
import com.apps.scit.e_shop.API.GetSortedProductsAPI;
import com.apps.scit.e_shop.Adapters.OurProductsAdapter;
import com.apps.scit.e_shop.Extra.EndlessRecyclerViewScrollListener;
import com.apps.scit.e_shop.Model.BaseResponse;
import com.apps.scit.e_shop.Model.Product;
import com.apps.scit.e_shop.Model.ProductsBaseObject;
import com.apps.scit.e_shop.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Products extends Fragment{
RecyclerView productsRecycler;
List<Product> listOfProducts;
OurProductsAdapter productsAdapter;
FloatingActionButton sortProducts;
    ProgressBar loadData,loadMore;
String[] choices;
int choosenItem = 0;
    private int pageNumber = 1;
    private int sortedPageNumber = 1;
    private int per_page = 10;
    Boolean isSorted;
    String sortingType;
    private static String BASE_URL;
    SharedPreferences typeShared;
    SharedPreferences.Editor editor;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.products_fragment,container,false);
        definingIDs(view);
        typeShared = getContext().getSharedPreferences("Sort",Context.MODE_PRIVATE);
        editor = typeShared.edit();
        isSorted = false;
        sortingType = typeShared.getString("AllProductsSort","by_name");;
        hideActivityElements();
        listOfProducts = new ArrayList<>();
        getProducts(pageNumber);
        productsAdapter = new OurProductsAdapter(getContext(),listOfProducts);
        final GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        productsRecycler.setLayoutManager(layoutManager);
        runAnimation(productsRecycler,0,productsAdapter);
        //productsRecycler.setAdapter(productsAdapter);
        productsRecycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
                    @Override
                    public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                        if (isSorted){
                            if (listOfProducts.size()>=per_page){
                                sortedPageNumber++;
                                performSortedPagination(sortedPageNumber,sortingType);
                            }
                        }else {
                            if (listOfProducts.size()>=per_page){
                                pageNumber++;
                                performPagination(pageNumber);
                            }
                        }
                    }
                });
        choices = new String[]{"الأحدث","الأعلى سعرا","الاسم","التقييم","الأكثر زيارة"};
        sortProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("قم باختيار طريقة الترتيب").setSingleChoiceItems(new ArrayAdapter<String>(getContext(), R.layout.rtl_list_item, R.id.text, choices),
                        convertSortTextToNum(typeShared.getString("AllProductsSort","by_name")), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        choosenItem = i;
                    }
                }).
                        setPositiveButton("حسنا", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                choosenItem = ((AlertDialog)dialogInterface).getListView().getCheckedItemPosition();;
                                switch (choosenItem){
                                    case 0:
                                    {
                                        isSorted = true;
                                        sortingType = "by_date";
                                        editor.putString("AllProductsSort","by_date");
                                        editor.commit();
                                        sortedPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(1,sortingType);
                                        break;
                                    }
                                    case 1:
                                    {
                                        isSorted = true;
                                        sortingType = "by_price";
                                        editor.putString("AllProductsSort","by_price");
                                        editor.commit();
                                        sortedPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(1,sortingType);
                                        break;
                                    }
                                    case 2:
                                    {
                                        isSorted = true;
                                        sortingType = "by_name";
                                        editor.putString("AllProductsSort","by_name");
                                        editor.commit();
                                        sortedPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(1,sortingType);
                                        break;
                                    }
                                    case 3:
                                    {
                                        isSorted = true;
                                        sortingType = "by_average";
                                        editor.putString("AllProductsSort","by_average");
                                        editor.commit();
                                        sortedPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(1,sortingType);
                                        break;
                                    }
                                    case 4:
                                    {
                                        isSorted = true;
                                        sortingType = "by_visiting";
                                        editor.putString("AllProductsSort","by_visiting");
                                        editor.commit();
                                        sortedPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(1,sortingType);
                                        break;
                                    }
                                }
                            }
                        }).show();
            }
        });
        return view;
    }

    /*
    @Override
    public boolean onBackPressed() {
        return true;
    }
    **/

    public class LineItemDecoration extends android.support.v7.widget.DividerItemDecoration {

        public LineItemDecoration(Context context, int orientation, @ColorInt int color) {
            super(context, orientation);

            setDrawable(new ColorDrawable(color));
        }

        public LineItemDecoration(Context context, int orientation, @NonNull Drawable drawable) {
            super(context, orientation);

            setDrawable(drawable);
        }
    }

    public void definingIDs(View view){
        //Base url
        BASE_URL = getResources().getString(R.string.base_url);
        //Floating Action Button
        sortProducts = view.findViewById(R.id.products_sort_products_fab);
        //RecyclerView
        productsRecycler = view.findViewById(R.id.products_recycler);
        //ProgressBars
        loadData = view.findViewById(R.id.products_load_data);
        loadMore = view.findViewById(R.id.products_load_more);
    }
    public void showActivityElements(){
        sortProducts.setVisibility(View.VISIBLE);
        productsRecycler.setVisibility(View.VISIBLE);
        loadData.setVisibility(View.GONE);
    }
    public void hideActivityElements(){
        sortProducts.setVisibility(View.GONE);
        productsRecycler.setVisibility(View.GONE);
        loadData.setVisibility(View.VISIBLE);
    }
    public void getProducts(int page){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        final GetAllProductsAPI getAllProductsAPI = retrofit.create(GetAllProductsAPI.class);
        Call<BaseResponse> call;
        call = getAllProductsAPI.getProducts(page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    ProductsBaseObject success = new Gson().fromJson(json,ProductsBaseObject.class);
                    per_page = success.getPer_page();
                    if (success.getData().size()>0){
                        for (Product p : success.getData()){
                            listOfProducts.add(p);
                            productsAdapter.notifyDataSetChanged();
                        }
                        showActivityElements();
                    }else {
                        Toast.makeText(getContext(), "لا يوجد منتجات", Toast.LENGTH_SHORT).show();
                        loadData.setVisibility(View.GONE);
                    }
                }else {
                    Toast.makeText(getContext(), "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                    loadData.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(getContext(), "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
                loadData.setVisibility(View.GONE);
            }
        });
    }

    public void getSortedProducts(int page,String type){
        loadData.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetSortedProductsAPI api = retrofit.create(GetSortedProductsAPI.class);
        Call<BaseResponse> call;
        call = api.getProducts(type,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    ProductsBaseObject success = new Gson().fromJson(json,ProductsBaseObject.class);
                    per_page = success.getPer_page();
                    if (success.getData().size()>0){
                        for (Product p : success.getData()){
                            listOfProducts.add(p);
                            productsAdapter.notifyDataSetChanged();
                        }
                        showActivityElements();
                    }else {
                        Toast.makeText(getContext(), "لا يوجد منتجات", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getContext(), "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(getContext(), "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void performPagination(int page){
        loadMore.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        final GetAllProductsAPI getAllProductsAPI = retrofit.create(GetAllProductsAPI.class);
        Call<BaseResponse> call;
        call = getAllProductsAPI.getProducts(page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    ProductsBaseObject success = new Gson().fromJson(json,ProductsBaseObject.class);
                    if (success.getData().size()>0){
                        for (Product p : success.getData()){
                            listOfProducts.add(p);
                            productsAdapter.notifyDataSetChanged();
                        }

                    }else {
                        Toast.makeText(getContext(), "لا يوجد منتجات إضافية", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getContext(), "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                }
                loadMore.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(getContext(), "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void performSortedPagination(int page,String type){
        loadMore.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetSortedProductsAPI api = retrofit.create(GetSortedProductsAPI.class);
        Call<BaseResponse> call;
        call = api.getProducts(type,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    ProductsBaseObject success = new Gson().fromJson(json,ProductsBaseObject.class);
                    if (success.getData().size()>0){
                        for (Product p : success.getData()){
                            listOfProducts.add(p);
                            productsAdapter.notifyDataSetChanged();
                        }

                    }else {
                        Toast.makeText(getContext(), "لا يوجد منتجات إضافية", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getContext(), "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                }
                loadMore.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(getContext(), "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
                loadMore.setVisibility(View.GONE);
            }
        });
    }
    public int convertSortTextToNum(String type){
        int output = 0;
        switch (type){
            case "by_date":output=0;break;
            case "by_price":output=1;break;
            case "by_name":output=2;break;
            case "by_average":output=3;break;
            case "by_visiting":output=4;break;
        }
        return output;
    }public void runAnimation(RecyclerView recyclerView,int type,OurProductsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
