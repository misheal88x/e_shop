package com.apps.scit.e_shop.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps.scit.e_shop.R;
public class We extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.we_fragment,container,false);
        return view;
    }

    /*
    @Override
    public boolean onBackPressed() {
        return true;
    }
    **/
}
