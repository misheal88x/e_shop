package com.apps.scit.e_shop.Model;

/**
 * Created by Misheal on 7/22/2018.
 */

public class SpecialOfferProduct {
    private int id;
    private String name;
    private String image;
    private String description;
    private String category_name;
    private String avgRate;

    public SpecialOfferProduct(int id, String name, String image, String description, String category_name, String avgRate) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.description = description;
        this.category_name = category_name;
        this.avgRate = avgRate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getAvgRate() {
        return avgRate;
    }

    public void setAvgRate(String avgRate) {
        this.avgRate = avgRate;
    }
}
