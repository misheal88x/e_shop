package com.apps.scit.e_shop.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 7/27/2018.
 */

public class SpecialOfferImage {
    @SerializedName("id") private int id = 0;
    @SerializedName("offer_id") private int offer_id = 0;
    @SerializedName("full_image_url") private String full_image_url = "";

    public SpecialOfferImage(int id, int offer_id, String full_image_url) {
        this.id = id;
        this.offer_id = offer_id;
        this.full_image_url = full_image_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(int offer_id) {
        this.offer_id = offer_id;
    }

    public String getImage_url() {
        return full_image_url;
    }

    public void setImage_url(String image_url) {
        this.full_image_url = image_url;
    }
}
