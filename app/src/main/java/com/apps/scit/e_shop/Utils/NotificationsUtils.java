package com.apps.scit.e_shop.Utils;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by Misheal on 6/21/2019.
 */

public class NotificationsUtils {
    public static String getFirebaseToken(){
        return FirebaseInstanceId.getInstance().getToken();
    }
}
