package com.apps.scit.e_shop.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.apps.scit.e_shop.API.GetProductAPI;
import com.apps.scit.e_shop.API.GetSpecialOfferAPI;
import com.apps.scit.e_shop.MainActivity;
import com.apps.scit.e_shop.Model.BaseResponse;
import com.apps.scit.e_shop.Model.ProductImage;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.apps.scit.e_shop.Adapters.RelatedProductsAdapter;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.Model.Offer;
import com.apps.scit.e_shop.Model.Product;
import com.apps.scit.e_shop.Model.SpecialOfferImage;
import com.apps.scit.e_shop.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OfferDetails extends AppCompatActivity implements com.glide.slider.library.Tricks.ViewPagerEx.OnPageChangeListener{
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    android.support.v7.widget.Toolbar toolbar;
    TextView toolbarTitle,information,relatedProducts,oldPrice,newPrice,remainingTime;
    AppBarLayout offer_details_appbar;
    ScrollView scrollView;
    RecyclerView relatedProductsRecycler;
    List<Product> listOfProducts;
    RelatedProductsAdapter offerProductsAdapter;
    com.glide.slider.library.SliderLayout slider;
    Database database;
    Offer offer;

    CountDownTimer timer;
    private static String BASE_URL = "";
    private ProgressBar load;
    Intent myIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details);
        definingIDs();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            offer_details_appbar.setOutlineProvider(null);
        }
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setBackgroundColor(getResources().getColor(R.color.transparentWhite));
        listOfProducts = new ArrayList<>();
        offerProductsAdapter = new RelatedProductsAdapter(this,listOfProducts);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        relatedProductsRecycler.setLayoutManager(layoutManager);
        runAnimation(relatedProductsRecycler,0);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.offer_details_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            this.finish();
        }
        return true;
    }
    public void definingIDs(){
        //ToolBar
        toolbar = findViewById(R.id.offer_details_toolbar);
        toolbarTitle = findViewById(R.id.basicToolbarTitle);
        offer_details_appbar = findViewById(R.id.offer_details_appbar);
        //Intent
        myIntent = getIntent();
        //Base Url
        BASE_URL = getResources().getString(R.string.base_url);
        //Progress Bar
        load = findViewById(R.id.offer_details_load);
        //Database
        database = new Database(OfferDetails.this);
        //Slider
        slider = findViewById(R.id.offer_details_slider);
        //ScrollView
        scrollView = findViewById(R.id.offer_details_scrollview);
        //TextViews
        information = findViewById(R.id.offer_details_name);
        newPrice = findViewById(R.id.offer_details_newPrice);
        oldPrice = findViewById(R.id.offer_details_oldPrice);
        relatedProducts = findViewById(R.id.offer_details_related_products);
        remainingTime = findViewById(R.id.offer_details_remaining);
        //RecyclerView
        relatedProductsRecycler = findViewById(R.id.offer_details_related_products_recycler);
        //Offer Object
        scrollView.setVisibility(View.GONE);
        callAPI(Integer.valueOf(myIntent.getStringExtra("offer_id")));
    }
    public void settingUpSlider(String type){
        ArrayList<String> listUrl = new ArrayList<>();
        ArrayList<String> listName = new ArrayList<>();
        if (type.equals("local")){
            int offer_id = Integer.valueOf(myIntent.getStringExtra("offer_id"));
            for (SpecialOfferImage s : database.getSpecialOfferImages(offer_id)){
                listUrl.add(s.getImage_url());
                listName.add(String.valueOf(s.getId()));
            }
        }else if (type.equals("online")){
            int index = 1;
            for (String s : offer.getImages()){
                listUrl.add(s);
                listName.add("name"+String.valueOf(index));
            }
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions
                .centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)

                .error(R.drawable.offline).placeholder(R.drawable.offlinecropped);
        for (int i = 0; i < listUrl.size(); i++) {
            com.glide.slider.library.SliderTypes.DefaultSliderView sliderView = new com.glide.slider.library.SliderTypes.DefaultSliderView(OfferDetails.this);
            sliderView
                    .image(listUrl.get(i))
                    .setBackgroundColor(getResources().getColor(R.color.lightGrey))
                    .setRequestOption(requestOptions)
                    .setProgressBarVisible(true);
            sliderView.bundle(new Bundle());
            sliderView.getBundle().putString("extra", listName.get(i));
            slider.addSlider(sliderView);
        }
        slider.setPresetTransformer(com.glide.slider.library.SliderLayout.Transformer.Default);
        slider.setPresetIndicator(com.glide.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new com.glide.slider.library.Animations.DescriptionAnimation());
        slider.stopAutoCycle();
        slider.addOnPageChangeListener(this);
        /*
        int offer_id = sharedPreferences.getInt("offer_id",0);
        if (database.getSpecialOfferImages(offer_id).size()>0){
            HashMap<String,String> url_maps = new HashMap<String, String>();
            for (SpecialOfferImage s : database.getSpecialOfferImages(offer_id)){
                url_maps.put(String.valueOf(s.getId()),s.getImage_url());
            }
            for (String name : url_maps.keySet()){
                DefaultSliderView defaultSliderView = new DefaultSliderView(OfferDetails.this);
                defaultSliderView
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop).error(R.drawable.offline).empty(R.drawable.offline).image(url_maps.get(name));
                defaultSliderView.bundle(new Bundle());
                defaultSliderView.getBundle()
                        .putString("extra",name);
                slider.addSlider(defaultSliderView);
            }
            slider.stopAutoCycle();
            slider.setPresetTransformer(SliderLayout.Transformer.Default);
            slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            slider.setCustomAnimation(new DescriptionAnimation());
            slider.addOnPageChangeListener(this);
            slider.setPresetTransformer("Default");
        }else {
            slider.setVisibility(View.GONE);
        }
        **/
    }
    public void getOfferProducts(String type){
        if (type.equals("local")){
            if (database.getSpecialOfferProducts(offer.getId()).size()>0){
                for ( Product p : database.getSpecialOfferProducts(offer.getId())){
                    listOfProducts.add(p);
                }
            }else {
                relatedProducts.setVisibility(View.GONE);
                relatedProductsRecycler.setVisibility(View.GONE);
            }
        }else if (type.equals("online")){
            if (offer.getProducts().size()>0){
                for ( Product p : offer.getProducts()){
                    listOfProducts.add(p);
                }
            }else {
                relatedProducts.setVisibility(View.GONE);
                relatedProductsRecycler.setVisibility(View.GONE);
            }
        }
        load.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);

    }
    public void settingUpMainData(){
        if (!offer.getOffer_remain().equals("")){
            final long leftTime = Long.parseLong(offer.getOffer_remain().substring(0,offer.getOffer_remain().length()-2));
            timer = new CountDownTimer(leftTime*1000,500) {
                @Override
                public void onTick(long l) {
                    remainingTime.setText(formatSeconds(l/1000));
                    database.updateSpecialOfferRemain(offer.getId(),l/1000);
                }

                @Override
                public void onFinish() {

                }
            }.start();
        }else {
           remainingTime.setText("00:00:00");
        }

        if (!offer.getInformation().equals("")){
            information.setText(offer.getInformation());
            getSupportActionBar().setTitle(offer.getInformation());
        }
        if (offer.getPrice()!=0){
            oldPrice.setText(offer.getPrice()+" ل.س");
        }
        if (offer.getOffer_price()!=0){
            newPrice.setText(offer.getOffer_price()+" ل.س");
        }
    }
    public  String formatSeconds(long totalSecs){
        long days = (int)(totalSecs/86400);
        long hours =  (int)((totalSecs % 86400) / 3600);
        long minutes =  (int)((totalSecs % 3600) / 60);
        long seconds =  (int)(totalSecs % 60);
        if (days==0){
            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        }else {
            return String.format("%02d:%02d:%02d", hours, minutes, seconds)+" و "+String.valueOf(days)+" يوم";
        }

    }
    public void runAnimation(RecyclerView recyclerView,int type){
         Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            relatedProductsRecycler.setAdapter(offerProductsAdapter);
            relatedProductsRecycler.setLayoutAnimation(controller);
            relatedProductsRecycler.getAdapter().notifyDataSetChanged();
            relatedProductsRecycler.scheduleLayoutAnimation();
        }
    }

    private void callAPI(final int id) {
        final SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetSpecialOfferAPI api = retrofit.create(GetSpecialOfferAPI.class);
        Call<BaseResponse> call;
        call=api.getInfo(String.valueOf(id));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body() != null){
                    if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                        String json = new Gson().toJson(response.body().getData());
                        Offer success = new Gson().fromJson(json,Offer.class);
                        offer = success;
                        settingUpMainData();
                        settingUpSlider("online");
                        getOfferProducts("online");
                    }else {
                        offer = database.getSingleSpecialOffer(id);
                        settingUpMainData();
                        settingUpSlider("local");
                        getOfferProducts("local");
                    }
                }else {
                    Intent intent = new Intent(OfferDetails.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                offer = database.getSingleSpecialOffer(id);
                settingUpMainData();
                settingUpSlider("local");
                getOfferProducts("local");
            }
        });
    }
}
