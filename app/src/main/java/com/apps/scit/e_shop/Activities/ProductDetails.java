package com.apps.scit.e_shop.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.scit.e_shop.API.GetProductPaginatedRatesAPI;
import com.apps.scit.e_shop.MainActivity;
import com.apps.scit.e_shop.Model.ConfigObject;
import com.apps.scit.e_shop.Model.ProductRatesResponse;
import com.apps.scit.e_shop.Model.SliderObject;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.apps.scit.e_shop.API.GetProductAPI;
import com.apps.scit.e_shop.API.RateProductAPI;
import com.apps.scit.e_shop.Adapters.CategoriesAdapter;
import com.apps.scit.e_shop.Adapters.CommentsAdapter;
import com.apps.scit.e_shop.Adapters.RelatedProductsAdapter;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.Model.BaseResponse;
import com.apps.scit.e_shop.Model.Product;
import com.apps.scit.e_shop.Model.ProductImage;
import com.apps.scit.e_shop.Model.ProductRate;
import com.apps.scit.e_shop.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductDetails extends AppCompatActivity implements com.glide.slider.library.Tricks.ViewPagerEx.OnPageChangeListener{
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
com.glide.slider.library.SliderLayout slider;
RecyclerView commentsRecycler,relatedProductsRecycler;
List<ProductRate> listOfComments;
List<Product> listOfProducts;
CommentsAdapter commentsAdapter;
RelatedProductsAdapter relatedProductsAdapter;
Button rateProduct,shareProduct;
    ImageView call,whatsapp;
    String phoneNumber = "";
Boolean isStarChoosed = false;
Boolean isCommentWritten = false;
android.support.v7.widget.Toolbar toolbar;
TextView toolbarTitle,productName,productAvailability,productoldPrice,productNewPrice,productDescription,userRateText,peopleRateNum,usersCommentstext,relatedProductsText;
RatingBar productRate,userRate;
AppBarLayout product_details_appbar;
NestedScrollView scrollView;
ProgressBar loadData;
List<String> listOfImages;
ProgressDialog sendRateProgress;
Database database;
private static String BASE_URL;
Boolean imagesFromNet = false;
int num_raters = 0;
int currentPage = 1;
Intent myIntent;
String product_price = "";
String offer_price = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        definingIDs();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            product_details_appbar.setOutlineProvider(null);
        }
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setBackgroundColor(getResources().getColor(R.color.transparentWhite));
        scrollView.setFocusableInTouchMode(true);
        scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        hideAllElements();
        loadData.setVisibility(View.VISIBLE);
        //Old price visibility
        final SharedPreferences sharedPreferences = getSharedPreferences("product",MODE_PRIVATE);
        if (myIntent.getStringExtra("productType").equals("offer")){
            productoldPrice.setVisibility(View.VISIBLE);
            //productoldPrice.setText("10000 ل.س");
        }else {
            productoldPrice.setVisibility(View.GONE);
        }
        listOfImages = new ArrayList<>();
        listOfComments = new ArrayList<>();
        listOfProducts = new ArrayList<>();
        callAPI();
        commentsAdapter = new CommentsAdapter(this,listOfComments);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        commentsRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        commentsRecycler.setLayoutManager(layoutManager);
        commentsRecycler.setAdapter(commentsAdapter);
        getPaginatedRates(currentPage);
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (listOfComments.size()>=20){
                            currentPage++;
                            getPaginatedRates(currentPage);
                        }
                    }
                }
            }
        });
        relatedProductsRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(ProductDetails.this,LinearLayoutManager.HORIZONTAL,false);
        relatedProductsRecycler.setLayoutManager(layoutManager1);
        relatedProductsAdapter = new RelatedProductsAdapter(ProductDetails.this,listOfProducts);
        runAnimation(relatedProductsRecycler,0);
        //relatedProductsRecycler.setAdapter(relatedProductsAdapter);
        relatedProductsAdapter.setOnItemClickListener(new CategoriesAdapter.OnItemClickListenter() {
            @Override
            public void onItemClick(int position) {
                Product product = listOfProducts.get(position);
                SharedPreferences sharedPreferences = getSharedPreferences("product",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("productType","product");
                editor.putInt("product_id",product.getId());
                editor.commit();
                Intent intent = new Intent(ProductDetails.this,ProductDetails.class);
                intent.putExtra("product_id",String.valueOf(product.getId()));
                intent.putExtra("productType","product");
                finish();
                startActivity(intent);
            }
        });
        rateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetails.this);
                View view1 = getLayoutInflater().inflate(R.layout.rate_dialog,null);
                final RatingBar stars = view1.findViewById(R.id.rate_dialog_stars);
                final EditText comment = view1.findViewById(R.id.rate_dialog_comment);
                final Button confirm = view1.findViewById(R.id.rate_dialog_confirm);
                confirm.setVisibility(View.INVISIBLE);
                builder.setView(view1);
                final AlertDialog dialog = builder.create();
                stars.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                        isStarChoosed = true;
                        confirm.setVisibility(View.VISIBLE);
                    }
                });
                confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String productType = myIntent.getStringExtra("productType");
                        int productId = Integer.valueOf(myIntent.getStringExtra("product_id"));
                        if (productType.equals("product")){
                            if (database.getProductIsRated(productId)==1){
                                Toast.makeText(ProductDetails.this, "قمت بالتقييم مسبقا لا يمكن التقييم مجددا!", Toast.LENGTH_SHORT).show();
                                dialog.cancel();
                            }else {
                                String commentText = comment.getText().toString();
                                if (commentText.length()<=3&&commentText.length()>=1){
                                    Toast.makeText(ProductDetails.this, "يجب أن يكون التعليق أربع محارف على الأقل", Toast.LENGTH_SHORT).show();
                                    dialog.cancel();
                                }else {
                                    if (commentText.equals("")){
                                        commentText = "لا يوجد تعليق";
                                    }
                                    sendRateProgress.show();
                                    final SharedPreferences sharedPreferences1 = getSharedPreferences("product",Context.MODE_PRIVATE);
                                    final int product_id = Integer.valueOf(myIntent.getStringExtra("product_id"));
                                    final SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
                                    final String token = sharedPreferences.getString("token","");
                                    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                                        @Override
                                        public okhttp3.Response intercept(Chain chain) throws IOException {
                                            Request newRequest  = chain.request().newBuilder()
                                                    .addHeader("Authorization", "Bearer " + token)
                                                    .build();
                                            return chain.proceed(newRequest);
                                        }
                                    }).build();
                                    Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
                                    RateProductAPI rateAPI = retrofit.create(RateProductAPI.class);
                                    Call<BaseResponse> call;
                                    call = rateAPI.rate(String.valueOf(stars.getRating()),commentText,product_id);
                                    call.enqueue(new Callback<BaseResponse>() {
                                        @Override
                                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                            if (response.body().getCode()==200&&response.body().getMessage_code()==105){
                                                sendRateProgress.cancel();
                                                Toast.makeText(ProductDetails.this, "تمت عملية التقييم بنجاح", Toast.LENGTH_SHORT).show();
                                                database.updateProductsIsRated(product_id,1);
                                                dialog.cancel();
                                                if (comment.getText().toString().equals("")){
                                                    listOfComments.add(new ProductRate((int)stars.getRating(),"لا يوجد تعليق",database.getProfile().getId()));
                                                }else {
                                                    listOfComments.add(new ProductRate((int)stars.getRating(),comment.getText().toString(),database.getProfile().getId()));
                                                }
                                                commentsAdapter.notifyDataSetChanged();
                                                showCommensSection();
                                                peopleRateNum.setText(String.valueOf(listOfComments.size())+" قامو بتقييم المنتج");
                                            }else {
                                                Toast.makeText(ProductDetails.this, "قمت بالتقييم مسبقا لا يمكن التقييم مجددا!", Toast.LENGTH_SHORT).show();
                                                dialog.cancel();
                                                sendRateProgress.cancel();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                                            Toast.makeText(ProductDetails.this, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
                                            dialog.cancel();
                                            sendRateProgress.cancel();
                                        }
                                    });

                                }

                            }
                        }else if (productType.equals("offer")){
                            if (database.getNormalOfferIsRated(productId)==1){
                                Toast.makeText(ProductDetails.this, "قمت بالتقييم مسبقا لا يمكن التقييم مجددا!", Toast.LENGTH_SHORT).show();
                                dialog.cancel();
                            }else {
                                String commentText = comment.getText().toString();
                                if (commentText.length()<=3){
                                    Toast.makeText(ProductDetails.this, "يجب أن يكون التعليق أربع محارف على الأقل", Toast.LENGTH_SHORT).show();
                                    dialog.cancel();
                                }else {
                                    if (commentText.equals("")){
                                        commentText = "لا يوجد تعليق";
                                    }
                                    sendRateProgress.show();
                                    final SharedPreferences sharedPreferences1 = getSharedPreferences("product",Context.MODE_PRIVATE);
                                    final int product_id = Integer.valueOf(myIntent.getStringExtra("product_id"));
                                    final SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
                                    final String token = sharedPreferences.getString("token","");
                                    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                                        @Override
                                        public okhttp3.Response intercept(Chain chain) throws IOException {
                                            Request newRequest  = chain.request().newBuilder()
                                                    .addHeader("Authorization", "Bearer " + token)
                                                    .build();
                                            return chain.proceed(newRequest);
                                        }
                                    }).build();
                                    Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
                                    RateProductAPI rateAPI = retrofit.create(RateProductAPI.class);
                                    Call<BaseResponse> call;
                                    call = rateAPI.rate(String.valueOf(stars.getRating()),commentText,product_id);
                                    call.enqueue(new Callback<BaseResponse>() {
                                        @Override
                                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                            if (response.body().getCode()==200&&response.body().getMessage_code()==105){
                                                sendRateProgress.cancel();
                                                Toast.makeText(ProductDetails.this, "تمت عملية التقييم بنجاح", Toast.LENGTH_SHORT).show();
                                                database.updateNormalOfferIsRated(product_id,1);
                                                dialog.cancel();
                                                if (comment.getText().toString().equals("")){
                                                    listOfComments.add(new ProductRate((int)stars.getRating(),"لا يوجد تعليق",database.getProfile().getId()));

                                                }else {
                                                    listOfComments.add(new ProductRate((int)stars.getRating(),comment.getText().toString(),database.getProfile().getId()));
                                                }
                                                commentsAdapter.notifyDataSetChanged();
                                                showCommensSection();
                                                peopleRateNum.setText(String.valueOf(listOfComments.size())+" قامو بتقييم المنتج");

                                            }else {
                                                Toast.makeText(ProductDetails.this, "قمت بالتقييم مسبقا لا يمكن التقييم مجددا!", Toast.LENGTH_SHORT).show();
                                                dialog.cancel();
                                                sendRateProgress.cancel();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                                            Toast.makeText(ProductDetails.this, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
                                            dialog.cancel();
                                            sendRateProgress.cancel();
                                        }
                                    });

                                }
                            }
                        }


                    }
                });
                dialog.show();
            }
        });
        shareProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences1 = getSharedPreferences("product",Context.MODE_PRIVATE);
                int product_id = Integer.valueOf(myIntent.getStringExtra("product_id"));
                String type = myIntent.getStringExtra("productType");
                Product p = null;
                if (type.equals("product")){
                    p = database.getSingleProduct(product_id);
                }else if (type.equals("offer")){
                    p = database.getSingleNormalOffer(product_id);
                }
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String body = new String();
                if (type.equals("product")){
                     body = "قم بالاطلاع على اخر منتجاتنا"+"\n"+p.getName()+"\n بسعر "+product_price+" ل.س"+"\nعلى تطبيق E-Shop"+"\n"+
                            "لمزيد من المعلومات على الرابط التالي :"+"\nhttps://www.e-shop.com";
                }else if (type.equals("offer")){
                     body = "قم بالاطلاع على اخر منتجاتنا"+"\n"+p.getName()+"\n بسعر "+offer_price+" ل.س"+"\nعلى تطبيق E-Shop"+"\n"+
                            "لمزيد من المعلومات على الرابط التالي :"+"\nhttps://www.e-shop.com";
                }

                intent.putExtra(Intent.EXTRA_TEXT,body);
                startActivity(Intent.createChooser(intent,"مشاركة عبر"));
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:0"+phoneNumber));
                startActivity(intent);
            }
        });
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://api.whatsapp.com/send?phone=+963 "+phoneNumber;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product_details_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            this.finish();
        }
        return true;
    }

    public void definingIDs(){
        //Base url
        BASE_URL = getResources().getString(R.string.base_url);
        //ToolBar
        toolbar = findViewById(R.id.product_details_toolbar);
        toolbarTitle = toolbar.findViewById(R.id.basicToolbarTitle);
        product_details_appbar = findViewById(R.id.product_details_appbar);
        //Intent
        myIntent = getIntent();
        //ScrollView
        scrollView = findViewById(R.id.product_details_scrollview);
        //Slider
        slider = findViewById(R.id.product_details_slider);
        //TextViews
        productName =findViewById(R.id.product_details_name);
        productAvailability = findViewById(R.id.product_details_availability);
        productNewPrice = findViewById(R.id.product_details_price);
        productoldPrice = findViewById(R.id.product_details_old_price);
        productDescription = findViewById(R.id.product_details_describtion);
        userRateText = findViewById(R.id.product_details_user_rate_value);
        peopleRateNum = findViewById(R.id.product_details_rate_num);
        usersCommentstext = findViewById(R.id.product_details_customer_thoughts_txt);
        relatedProductsText = findViewById(R.id.product_details_related_products_txt);
        //Buttons
        rateProduct = findViewById(R.id.product_details_star);
        shareProduct = findViewById(R.id.product_details_share);
        //ImageViews
        call = findViewById(R.id.product_details_call);
        whatsapp = findViewById(R.id.product_details_whatsapp);
        //RatingBars
        productRate = findViewById(R.id.product_details_rate);
        userRate = findViewById(R.id.product_details_user_rate);
        //RecyclerViews
        commentsRecycler = findViewById(R.id.product_details_users_comments);
        relatedProductsRecycler = findViewById(R.id.product_details_related_products_recycler);
        //ProgressBar
        loadData = findViewById(R.id.product_details_load_data);
        //Database
        database = new Database(ProductDetails.this);
        //Progress Dialog
        sendRateProgress = new ProgressDialog(ProductDetails.this);
        sendRateProgress.setTitle("التقييم");
        sendRateProgress.setMessage("جار إرسال التقييم");
        //Phone number
        if (database.getConfigurations(0)!=null){
            if (database.getConfigurations(0).size()>0){
                for (ConfigObject co : database.getConfigurations(0)){
                    if (co.getName().equals("Mobile")){
                        phoneNumber = co.getValue();
                    }
                }
            }
        }
    }
    public void setupSlider(int product_id){
        ArrayList<String> listUrl = new ArrayList<>();
        ArrayList<String> listName = new ArrayList<>();
            if (imagesFromNet){
                int j = 1;
                for (String i : listOfImages){
                    listUrl.add(i);
                    listName.add(String.valueOf(j));
                    j++;
                }
            }else {
                int j = 1;
                for (ProductImage i : database.getProductImages(product_id)){
                    listUrl.add(i.getImage());
                    listName.add(String.valueOf(j));
                    j++;
                }
            }

            RequestOptions requestOptions = new RequestOptions();
            requestOptions
                    .centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.offline).placeholder(R.drawable.offlinecropped);
            for (int i = 0; i < listUrl.size(); i++) {
                com.glide.slider.library.SliderTypes.DefaultSliderView sliderView = new com.glide.slider.library.SliderTypes.DefaultSliderView(ProductDetails.this);
                sliderView
                        .image(listUrl.get(i))
                        .setBackgroundColor(getResources().getColor(R.color.lightGrey))
                        .setRequestOption(requestOptions)
                        .setProgressBarVisible(true);
                sliderView.bundle(new Bundle());
                sliderView.getBundle().putString("extra", listName.get(i));
                slider.addSlider(sliderView);
            }
            slider.setPresetTransformer(com.glide.slider.library.SliderLayout.Transformer.Default);
            slider.setPresetIndicator(com.glide.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
            slider.setCustomAnimation(new com.glide.slider.library.Animations.DescriptionAnimation());
            slider.stopAutoCycle();
            slider.addOnPageChangeListener(this);

        /*
        HashMap<String,String> url_maps = new HashMap<String, String>();
        for (ProductImage i : listOfImages){
            url_maps.put(String.valueOf(i.getId()),i.getImage());
        }
        for(String name : url_maps.keySet()){
            DefaultSliderView defaultSliderView = new DefaultSliderView(ProductDetails.this);
            defaultSliderView
                    .image(url_maps.get(name)).empty(R.drawable.offline).error(R.drawable.offline)
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop);
            defaultSliderView.bundle(new Bundle());
            defaultSliderView.getBundle()
                    .putString("extra",name);

            slider.addSlider(defaultSliderView);
        }
        slider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.stopAutoCycle();
        slider.addOnPageChangeListener(this);
        slider.setPresetTransformer("Default");
        **/
    }

    private void callAPI() {
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        final SharedPreferences sharedPreferences1 = getSharedPreferences("product",Context.MODE_PRIVATE);
        final int product_id = Integer.valueOf(myIntent.getStringExtra("product_id"));
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetProductAPI api = retrofit.create(GetProductAPI.class);
        Call<BaseResponse> call;
        call=api.getProduct(myIntent.getStringExtra("product_id"));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body() != null){
                    if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                        String json = new Gson().toJson(response.body().getData());
                        Product success = new Gson().fromJson(json,Product.class);
                        //Slider
                        if (!success.getImage().equals("")&&success.getImages().size()>0){
                            imagesFromNet = true;
                            database.deleteProductImages(product_id);
                            database.insertProductImage(product_id,success.getImage(),product_id);
                            ProductImage image = new ProductImage(999999+success.getId(),success.getImage(),success.getId());
                            listOfImages.add(image.getImage());
                            for (String i : success.getImages()){
                                database.insertProductImage(0,i,product_id);
                                listOfImages.add(i);
                            }
                        }else {
                            if (!success.getImage().equals("")){
                                imagesFromNet = true;
                                database.deleteProductImages(product_id);
                                database.insertProductImage(product_id,success.getImage(),product_id);
                                ProductImage image = new ProductImage(999999+success.getId(),success.getImage(),success.getId());
                                listOfImages.add(image.getImage());
                            }
                            if (success.getImages().size()>0){
                                imagesFromNet = true;
                                database.deleteProductImages(product_id);
                                for (String i : success.getImages()){
                                    listOfImages.add(i);
                                    database.insertProductImage(0,i,product_id);
                                }
                            }
                        }
                        setupSlider(product_id);
                        //todo get rates
                    /*
                    //Comments
                    if (success.getRates().size()>0){
                        peopleRateNum.setText(String.valueOf(success.getRates().size())+" قامو بتقييم المنتج");
                        for (ProductRate r : success.getRates()){
                            listOfComments.add(r);
                        }
                    }else {
                       hideCommentsSection();
                    }
                    **/
                        //Related Products
                        if (success.getRelated_products().size()>0){
                            for (Product p : success.getRelated_products()){
                                listOfProducts.add(p);
                            }
                        }else {
                            hideRelatedProductsSection();
                        }
                        if (myIntent.getStringExtra("productType").equals("product")){
                            //todo get available
                            //Update previous product informations
                            database.updateProduct(success.getId(),success.getName(),success.getImage(),success.getDescription(),success.getPrice(),success.getAvgRate(),
                                    success.getOffer_price(),success.getIs_offer(),success.getCategory_name(),success.getAvailable());
                        }else {
                            //Update previous offer informations
                            database.updateNormalOffer(success.getId(),success.getName(),success.getImage(),success.getDescription(),success.getPrice(),success.getCategory_name(),success.getAvgRate(),
                                    success.getOffer_price(),success.getIs_offer(),success.getAvailable());
                        }

                        //setup product informations
                        //product name
                        productName.setText(success.getName());
                        getSupportActionBar().setTitle(success.getName());
                        //availability
                        if (success.getAvailable()==0){
                            productAvailability.setText("غير متوفر");
                            productAvailability.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                        }else {
                            productAvailability.setText("متوفر");
                        }
                        //Old Price and New Price
                        productoldPrice.setText(String.valueOf(success.getPrice())+" ل.س");
                        product_price = String.valueOf(success.getPrice());
                        productNewPrice.setText(String.valueOf(success.getOffer_price())+" ل.س");
                        offer_price = String.valueOf(success.getOffer_price());
                        //Product Rate
                        try{
                            productRate.setRating(Float.parseFloat(success.getAvgRate()));
                            userRate.setRating(Float.parseFloat(success.getAvgRate()));
                            userRateText.setText(String.valueOf(Math.round(Float.valueOf(success.getAvgRate()))));
                        }catch (Exception e){}
                        //product description
                        productDescription.setText(success.getDescription());
                        showAllElements();
                    }else{
                        hideCommentsSection();
                        hideRelatedProductsSection();
                        Product p = null;
                        if (myIntent.getStringExtra("productType").equals("product")){
                            p = database.getSingleProduct(product_id);
                        }else {
                            p = database.getSingleNormalOffer(product_id);
                        }
                        //product name
                        productName.setText(p.getName());
                        getSupportActionBar().setTitle(p.getName());
                        //availability
                        if (p.getAvailable()==0){
                            productAvailability.setText("غير متوفر");
                            productAvailability.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                        }else {
                            productAvailability.setText("متوفر");
                        }
                        //Old Price and New Price
                        productoldPrice.setText(String.valueOf(p.getPrice())+" ل.س");
                        productNewPrice.setText(String.valueOf(p.getOffer_price())+" ل.س");
                        product_price = String.valueOf(p.getPrice());
                        offer_price = String.valueOf(p.getOffer_price());
                        try{
                            productRate.setRating(Float.parseFloat(p.getAvgRate()));
                            userRate.setRating(Float.parseFloat(p.getAvgRate()));
                            userRateText.setText(p.getAvgRate());
                        }catch (Exception e){}
                        //product description
                        productDescription.setText(p.getDescription());
                        showAllElements();
                    }
                }else {
                    Intent intent = new Intent(ProductDetails.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                loadData.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                hideCommentsSection();
                hideRelatedProductsSection();
                Product p = null;
                if (myIntent.getStringExtra("productType").equals("product")){
                    p = database.getSingleProduct(product_id);
                }else {
                    p = database.getSingleNormalOffer(product_id);
                }
                setupSlider(product_id);
                //product name
                productName.setText(p.getName());
                getSupportActionBar().setTitle(p.getName());
                //availability
                if (p.getAvailable()==0){
                    productAvailability.setText("غير متوفر");
                    productAvailability.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                }else {
                    productAvailability.setText("متوفر");
                }
                //Old Price and New Price
                productoldPrice.setText(String.valueOf(p.getPrice())+" ل.س");
                productNewPrice.setText(String.valueOf(p.getOffer_price())+" ل.س");
                product_price = String.valueOf(p.getPrice());
                offer_price = String.valueOf(p.getOffer_price());
                try{
                    productRate.setRating(Float.parseFloat(p.getAvgRate()));
                    userRate.setRating(Float.parseFloat(p.getAvgRate()));
                    userRateText.setText(p.getAvgRate());
                }catch (Exception e){}
                //product description
                productDescription.setText(p.getDescription());
                loadData.setVisibility(View.GONE);
                showAllElements();

            }
        });
    }

    private void getPaginatedRates(final int page){
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        final SharedPreferences sharedPreferences1 = getSharedPreferences("product",Context.MODE_PRIVATE);
        final int product_id = Integer.valueOf(myIntent.getStringExtra("product_id"));
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetProductPaginatedRatesAPI api = retrofit.create(GetProductPaginatedRatesAPI.class);
        Call<BaseResponse> call;
        call = api.getRatings(myIntent.getStringExtra("product_id"),page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode() == 200 && response.body().getMessage_code() == 108) {
                    String json = new Gson().toJson(response.body().getData());
                    ProductRatesResponse success = new Gson().fromJson(json, ProductRatesResponse.class);
                    if (success.getData() != null) {
                        if (success.getData().size() > 0) {
                            if (page == 1) {
                                num_raters = num_raters + success.getData().size();
                                peopleRateNum.setText(String.valueOf(num_raters) + " قامو بتقييم المنتج");
                                for(ProductRate pr : success.getData()){
                                    listOfComments.add(pr);
                                    commentsAdapter.notifyDataSetChanged();
                                }
                            }
                        }else {
                            if (page == 1) {
                                hideCommentsSection();
                            } else {
                                Toast.makeText(ProductDetails.this, "لا يوجد تقييمات إضافية", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Toast.makeText(ProductDetails.this, "حدث خطأ ما", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(ProductDetails.this, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void hideAllElements(){
        scrollView.setVisibility(View.GONE);
    }
    public void showAllElements(){
        scrollView.setVisibility(View.VISIBLE);
    }
    public void hideCommentsSection(){
        usersCommentstext.setVisibility(View.GONE);
        commentsRecycler.setVisibility(View.GONE);
    }
    public void showCommensSection(){
        usersCommentstext.setVisibility(View.VISIBLE);
        commentsRecycler.setVisibility(View.VISIBLE);
    }
    public void hideRelatedProductsSection(){
        relatedProductsText.setVisibility(View.GONE);
        relatedProductsRecycler.setVisibility(View.GONE);
    }
    public void showRelatedProductsSection(){
        relatedProductsText.setVisibility(View.VISIBLE);
        relatedProductsRecycler.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    public void runAnimation(RecyclerView recyclerView,int type){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            relatedProductsRecycler.setAdapter(relatedProductsAdapter);
            relatedProductsRecycler.setLayoutAnimation(controller);
            relatedProductsRecycler.getAdapter().notifyDataSetChanged();
            relatedProductsRecycler.scheduleLayoutAnimation();
        }
    }
}
