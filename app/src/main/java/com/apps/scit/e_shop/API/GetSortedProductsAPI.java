package com.apps.scit.e_shop.API;

import com.apps.scit.e_shop.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 7/31/2018.
 */

public interface GetSortedProductsAPI {
    @GET("api/v1/product/byFilter/{type}")
    Call<BaseResponse> getProducts(@Path("type")String type,
                                   @Query("page")int page);
}
