package com.apps.scit.e_shop.API;

import com.apps.scit.e_shop.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 7/26/2018.
 */

public interface GetCategoryProductsAPI {
    @GET("api/v1/category/{catid}")
    Call<BaseResponse> getProducts(@Path("catid")String catId,
                                   @Query("page")int page);
}
