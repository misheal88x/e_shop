package com.apps.scit.e_shop.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 7/26/2018.
 */

public class CategoryBaseObject {
    @SerializedName("id") private int id;
    @SerializedName("name") private String name;
    @SerializedName("full_image_url") private String full_image_url;
    @SerializedName("full_icon_image_url") private String full_icon_image_url;
    @SerializedName("parent_id") private int parent_id;
    @SerializedName("products") private ProductsBaseObject products;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return full_image_url;
    }

    public void setImage(String image) {
        this.full_image_url = image;
    }

    public String getIcon_image() {
        return full_icon_image_url;
    }

    public void setIcon_image(String icon_image) {
        this.full_icon_image_url = icon_image;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public ProductsBaseObject getProducts() {
        return products;
    }

    public void setProducts(ProductsBaseObject products) {
        this.products = products;
    }
}
