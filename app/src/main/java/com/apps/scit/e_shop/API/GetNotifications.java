package com.apps.scit.e_shop.API;

import com.apps.scit.e_shop.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Misheal on 6/22/2019.
 */

public interface GetNotifications {
    @GET("api/v1/notifications")
    Call<BaseResponse> getNotifications(@Query("page") int page);
}
