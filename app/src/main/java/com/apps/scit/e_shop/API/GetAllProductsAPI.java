package com.apps.scit.e_shop.API;

import com.apps.scit.e_shop.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Misheal on 7/25/2018.
 */

public interface GetAllProductsAPI {
    @GET("api/v1/product/")
    Call<BaseResponse> getProducts(@Query("page") int page);
}
