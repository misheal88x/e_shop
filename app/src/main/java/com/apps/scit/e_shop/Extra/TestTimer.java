package com.apps.scit.e_shop.Extra;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.apps.scit.e_shop.R;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.DefaultSliderView;
import com.glide.slider.library.Tricks.ViewPagerEx;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TestTimer extends AppCompatActivity implements ViewPagerEx.OnPageChangeListener,BaseSliderView.OnSliderClickListener{
SliderLayout sliderLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_timer);
sliderLayout = findViewById(R.id.slidertest);
        ArrayList<String> listUrl = new ArrayList<>();
        ArrayList<String> listName = new ArrayList<>();

        listUrl.add("https://www.revive-adserver.com/media/GitHub.jpg");
        listName.add("JPG - Github");

        listUrl.add("https://tctechcrunch2011.files.wordpress.com/2017/02/android-studio-logo.png");
        listName.add("PNG - Android Studio");

        listUrl.add("http://static.tumblr.com/7650edd3fb8f7f2287d79a67b5fec211/3mg2skq/3bdn278j2/tumblr_static_idk_what.gif");
        listName.add("GIF - Disney");

        listUrl.add("http://www.gstatic.com/webp/gallery/1.webp");
        listName.add("WEBP - Mountain");

        listUrl.add("https://www.clicktorelease.com/tmp/google-svg/image.svg");
        listName.add("SVG - Google");
        RequestOptions requestOptions = new RequestOptions();
        requestOptions
                .centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.offlinecropped);
        for (int i = 0; i < listUrl.size(); i++) {
            DefaultSliderView sliderView = new DefaultSliderView(this);
            sliderView
                    .image(listUrl.get(i))

                    .setRequestOption(requestOptions)
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true)
                    .setOnSliderClickListener(this);;
            sliderView.bundle(new Bundle());
            sliderView.getBundle().putString("extra", listName.get(i));

            sliderLayout.addSlider(sliderView);
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);

        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(4000);
        sliderLayout.addOnPageChangeListener(this);
        /*
        String dtStart = "2018-05-01 09:27:37";
        String newStartDate = dtStart.replace(dtStart.substring(0,10),"2018-05-05");
        String dtEnd = "2018-05-05 10:27:37";
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //Date date1 = null;
       // Date date2 = null;
        Date time1 = null;
        Date time2 = null;
        try {
           // date1 = dateFormat.parse(dtStart);
           // date2 = dateFormat.parse(dtEnd);
            time1 = timeFormat.parse(newStartDate);
            time2 = timeFormat.parse(dtEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //long diff = date2.getTime() - date1.getTime();
        long diff = time2.getTime() - time1.getTime();
        //textView.setText("Days: " + String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)));
        textView.setText("Seconds: "+String.valueOf(TimeUnit.MILLISECONDS.toMillis(diff)));

        //textView.setText(String.valueOf(getMilliSeconds("2018-08-05 22:30:00")));
        timer = new CountDownTimer(50000,1000) {
            @Override
            public void onTick(long l) {
               textView.setText(formatSeconds(l/1000));
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }
    public String getDays(String inputDate){
        SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String myDate = format.format(new Date());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        Date todayDate = null;
        Date offerDate = null;
        try{
            todayDate = dateFormat.parse(myDate);
            offerDate = dateFormat.parse(inputDate);
        }catch (ParseException e){
            e.printStackTrace();
        }
        long diff = (offerDate.getTime() - todayDate.getTime());
        return String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
    }
    public long getMilliSeconds(String inputDate){
        //String newInputtDate = inputDate.replace(inputDate.substring(0,10),"2018-05-05");
        SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String myDate = format.format(new Date());
        //String newTodayDate = myDate.replace(myDate.substring(0,10),"2018-05-05");
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date todayDate = null;
        Date offerDate = null;

        try {
            todayDate = timeFormat.parse(myDate);
            offerDate = timeFormat.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = offerDate.getTime() - todayDate.getTime();
        return TimeUnit.MILLISECONDS.toSeconds(diff);
    }
    public  String formatSeconds(long totalSecs){
        long hours =  totalSecs / 3600;
        long minutes =  (totalSecs % 3600) / 60;
        long seconds =  totalSecs % 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
    **/
    }
    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        sliderLayout.stopAutoCycle();
        super.onStop();
    }
    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onSliderClick(BaseSliderView baseSliderView) {

    }
}
