package com.apps.scit.e_shop.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import com.apps.scit.e_shop.Activities.CategoryProducts;
import com.apps.scit.e_shop.Adapters.CategoriesAdapter;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.Extra.IOnBackPressed;
import com.apps.scit.e_shop.Model.Category;
import com.apps.scit.e_shop.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;


public class Categories extends Fragment implements IOnBackPressed {
    RecyclerView categoriesRecycler;
    List<Category> listOfCategories;
    CategoriesAdapter categoriesAdapter;
    Database database;
    Stack<Integer> parent_id_stack;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.categories_fragment,container,false);
        database = new Database(getContext());
        categoriesRecycler = view.findViewById(R.id.category_recycler);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("Extra",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("lastBackPage",false);
        editor.commit();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        categoriesRecycler.setLayoutManager(layoutManager);
        listOfCategories = new ArrayList<>();
        parent_id_stack = new Stack<>();
        parent_id_stack.push(0);
        if (database.getCategories(0).size()>0){
            for (Category c : database.getCategories(0)){
                listOfCategories.add(c);
            }
        }else {
            Toast.makeText(getContext(), "لا يوجد أصناف!", Toast.LENGTH_SHORT).show();
        }
        categoriesAdapter = new CategoriesAdapter(getContext(),listOfCategories);
        runAnimation(categoriesRecycler,0,categoriesAdapter);
        //categoriesRecycler.setAdapter(categoriesAdapter);
        categoriesAdapter.setOnItemClickListener(new CategoriesAdapter.OnItemClickListenter() {
            @Override
            public void onItemClick(int position) {
                if (database.getCategories(listOfCategories.get(position).getId()).size()>0){
                    parent_id_stack.push(listOfCategories.get(position).getId());
                    int id = listOfCategories.get(position).getId();
                    listOfCategories.clear();
                    for (Category c : database.getCategories(id)){
                        listOfCategories.add(c);
                        categoriesAdapter.notifyDataSetChanged();
                    }
                }else {
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Category", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor= sharedPreferences.edit();
                    editor.putInt("category_id",listOfCategories.get(position).getId());
                    editor.putString("category_name",listOfCategories.get(position).getName());
                    editor.commit();
                    Intent intent = new Intent(getContext(),CategoryProducts.class);
                    startActivity(intent);
                }

            }
        });
        return view;
    }

    @Override
    public boolean onBackPressed() {
        if (parent_id_stack.peek()==0){
            try {
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("Extra", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("lastBackPage", true);
                editor.commit();
                return true;
            }catch (Exception e){}finally {
                return true;
            }
        }else {
            parent_id_stack.pop();
            listOfCategories.clear();
            for (Category c : database.getCategories(parent_id_stack.peek())){
                listOfCategories.add(c);
                categoriesAdapter.notifyDataSetChanged();
            }
            return true;
        }

    }
    public void runAnimation(RecyclerView recyclerView,int type,CategoriesAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
