package com.apps.scit.e_shop.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.scit.e_shop.API.GetCategoryProductsAPI;
import com.apps.scit.e_shop.API.GetSortedCategoryProductsAPI;
import com.apps.scit.e_shop.Adapters.MostSearchedCategoriesAdapter;
import com.apps.scit.e_shop.Adapters.OurProductsAdapter;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.Extra.EndlessRecyclerViewScrollListener;
import com.apps.scit.e_shop.Model.BaseResponse;
import com.apps.scit.e_shop.Model.Category;
import com.apps.scit.e_shop.Model.CategoryBaseObject;
import com.apps.scit.e_shop.Model.Product;
import com.apps.scit.e_shop.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CategoryProducts extends AppCompatActivity {
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    Toolbar toolbar;
    TextView toolbarTitle;
FloatingActionButton sortFab;
RecyclerView productsRecycler;
List<Product> listOfProducts;
OurProductsAdapter adapter;
String[] choices;
int choosenItem = 0;
ProgressBar loadData,loadMore;
private Boolean isSorted;
private String sortType;
    private int pageNumber = 1;
    private int sortPageNumber = 1;
    private int per_page = 20;
    private static String BASE_URL;
    SharedPreferences typeShared;
    SharedPreferences.Editor editor;
    Database database;
    //No Data Layout Elements
    LinearLayout noDataLayout;
    ImageView noDataImage;
    TextView noDataText1,noDataText2;
    RecyclerView noDatarecycler;
    List<Category> noDataListCategories;
    MostSearchedCategoriesAdapter noDataAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_products);
        if (getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_LTR){
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
        definingIDs();

        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getSupportActionBar().setTitle("");
        typeShared = getSharedPreferences("Sort",Context.MODE_PRIVATE);
        editor = typeShared.edit();
        isSorted = false;
        sortType = typeShared.getString("CategoryProductsSort","by_name");
        SharedPreferences sharedPreferences = getSharedPreferences("Category",Context.MODE_PRIVATE);
        toolbarTitle.setText(sharedPreferences.getString("category_name",""));
        hideActivityElements();
        listOfProducts = new ArrayList<>();
        getProducts(pageNumber);
        adapter = new OurProductsAdapter(this,listOfProducts);
        final GridLayoutManager layoutManager = new GridLayoutManager(this,2);
        //productsRecycler.setAdapter(adapter);
        productsRecycler.setLayoutManager(layoutManager);
        runAnimation(productsRecycler,0,adapter);
        productsRecycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (isSorted){
                    if (listOfProducts.size()>=per_page){
                        sortPageNumber++;
                        performSortedPagination(sortType,sortPageNumber);
                    }
                }else {
                    if (listOfProducts.size()>=per_page){
                        pageNumber++;
                        performPagination(pageNumber);
                    }
                }
            }
        });
        choices = new String[]{"الأحدث","الأعلى سعرا","الاسم","التقييم","الأكثر زيارة"};
        sortFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(CategoryProducts.this);
                builder.setTitle("قم باختيار طريقة الترتيب").setSingleChoiceItems(new ArrayAdapter<String>(CategoryProducts.this, R.layout.rtl_list_item, R.id.text, choices),
                        convertSortTextToNum(typeShared.getString("CategoryProductsSort","by_name")), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        choosenItem = i;
                    }
                }).
                        setPositiveButton("حسنا", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                choosenItem = ((AlertDialog)dialogInterface).getListView().getCheckedItemPosition();;
                                switch (choosenItem){
                                    case 0:
                                    {
                                        isSorted = true;
                                        sortType = "by_date";
                                        editor.putString("CategoryProductsSort","by_date");
                                        editor.commit();
                                        sortPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(sortType,1);
                                        break;
                                    }
                                    case 1:
                                    {
                                        isSorted = true;
                                        sortType = "by_price";
                                        editor.putString("CategoryProductsSort","by_price");
                                        editor.commit();
                                        sortPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(sortType,1);
                                        break;
                                    }
                                    case 2:
                                    {
                                        isSorted = true;
                                        sortType = "by_name";
                                        editor.putString("CategoryProductsSort","by_name");
                                        editor.commit();
                                        sortPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(sortType,1);
                                        break;
                                    }
                                    case 3:
                                    {
                                        isSorted = true;
                                        sortType = "by_average";
                                        editor.putString("CategoryProductsSort","by_average");
                                        editor.commit();
                                        sortPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(sortType,1);
                                        break;
                                    }
                                    case 4:
                                    {
                                        isSorted = true;
                                        sortType = "by_visiting";
                                        editor.putString("CategoryProductsSort","by_visiting");
                                        editor.commit();
                                        sortPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(sortType,1);
                                        break;
                                    }
                                }
                            }
                        }).show();
            }
        });

    }



    public class LineItemDecoration extends android.support.v7.widget.DividerItemDecoration {

        public LineItemDecoration(Context context, int orientation, @ColorInt int color) {
            super(context, orientation);

            setDrawable(new ColorDrawable(color));
        }

        public LineItemDecoration(Context context, int orientation, @NonNull Drawable drawable) {
            super(context, orientation);

            setDrawable(drawable);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.category_products_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            this.finish();
        }
        return true;
    }
    private void definingIDs() {
        //Base url
        BASE_URL = getResources().getString(R.string.base_url);
        //ToolBar
        toolbar = findViewById(R.id.category_product_toolbar);
        toolbarTitle = toolbar.findViewById(R.id.basicToolbarTitle);
        //Floating Action Button
        sortFab = findViewById(R.id.sort_products_fab);
        //RecyclerView
        productsRecycler = findViewById(R.id.category_products_recycler);
        //ProgressBars
        loadData = findViewById(R.id.category_products_load_data);
        loadMore = findViewById(R.id.category_products_load_more);
        //Database
        database = new Database(CategoryProducts.this);

        //No Data Elements
        noDataLayout = findViewById(R.id.category_products_no_data_layout);
        noDataImage = findViewById(R.id.category_products_no_data_image);
        noDataText1 = findViewById(R.id.category_products_no_data_text1);
        noDataText2 = findViewById(R.id.category_products_no_data_text2);
        noDatarecycler = findViewById(R.id.category_products_no_data_recycler);
        noDataListCategories = new ArrayList<>();
        RecyclerView.LayoutManager noDataLayoutManager = new LinearLayoutManager(CategoryProducts.this,LinearLayoutManager.HORIZONTAL,false);
        noDatarecycler.setLayoutManager(noDataLayoutManager);
    }
    public void showActivityElements(){
        sortFab.setVisibility(View.VISIBLE);
        productsRecycler.setVisibility(View.VISIBLE);
        loadData.setVisibility(View.GONE);
    }
    public void hideActivityElements(){
        sortFab.setVisibility(View.GONE);
        productsRecycler.setVisibility(View.GONE);
        loadData.setVisibility(View.VISIBLE);
    }
    public void getProducts(int page){
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        SharedPreferences sharedPreferences1 = getSharedPreferences("Category",Context.MODE_PRIVATE);
        final int category_id = sharedPreferences1.getInt("category_id",0);
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetCategoryProductsAPI getAllProductsAPI = retrofit.create(GetCategoryProductsAPI.class);
        Call<BaseResponse> call;
        call = getAllProductsAPI.getProducts(String.valueOf(category_id),page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    CategoryBaseObject success = new Gson().fromJson(json,CategoryBaseObject.class);
                    per_page = success.getProducts().getPer_page();
                    if (success.getProducts().getData().size()>0){
                        for (Product p : success.getProducts().getData()){
                            listOfProducts.add(p);
                            adapter.notifyDataSetChanged();
                        }
                        showActivityElements();
                    }else {

                        Toast.makeText(CategoryProducts.this, "لا يوجد منتجات", Toast.LENGTH_SHORT).show();
                        if (database.getCategories(database.getCategoryParent(category_id)).size()>0){
                            for (Category c: database.getCategories(database.getCategoryParent(category_id))) {
                                if (c.getId()!=category_id){
                                    noDataListCategories.add(c);
                                }
                            }
                            noDataAdapter = new MostSearchedCategoriesAdapter(CategoryProducts.this,noDataListCategories);
                            noDatarecycler.setAdapter(noDataAdapter);
                            noDataLayout.setVisibility(View.VISIBLE);
                        }else {
                            noDataLayout.setVisibility(View.VISIBLE);
                            noDataText2.setVisibility(View.GONE);
                            noDatarecycler.setVisibility(View.GONE);
                        }
                        loadData.setVisibility(View.GONE);
                    }
                }else {
                    Toast.makeText(CategoryProducts.this, "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                    if (database.getCategories(database.getCategoryParent(category_id)).size()>0){
                        for (Category c: database.getCategories(database.getCategoryParent(category_id))) {
                            if (c.getId()!=category_id) {
                                noDataListCategories.add(c);
                            }
                        }
                        noDataAdapter = new MostSearchedCategoriesAdapter(CategoryProducts.this,noDataListCategories);
                        noDatarecycler.setAdapter(noDataAdapter);
                        noDataLayout.setVisibility(View.VISIBLE);
                        noDataText1.setVisibility(View.GONE);
                        loadData.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(CategoryProducts.this, "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
                if (database.getCategories(database.getCategoryParent(category_id)).size()>0){
                    for (Category c: database.getCategories(database.getCategoryParent(category_id))) {
                        if (c.getId()!=category_id){
                            noDataListCategories.add(c);
                        }

                    }
                    noDataAdapter = new MostSearchedCategoriesAdapter(CategoryProducts.this,noDataListCategories);
                    noDatarecycler.setAdapter(noDataAdapter);
                    noDataLayout.setVisibility(View.VISIBLE);
                    noDataText1.setVisibility(View.GONE);
                    loadData.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void getSortedProducts(String sort, int page){
        loadData.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        SharedPreferences sharedPreferences1 = getSharedPreferences("Category",Context.MODE_PRIVATE);
        int category_id = sharedPreferences1.getInt("category_id",0);
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetSortedCategoryProductsAPI getAllSortedProductsAPI = retrofit.create(GetSortedCategoryProductsAPI.class);
        Call<BaseResponse> call;
        call = getAllSortedProductsAPI.getProducts(String.valueOf(category_id),sort,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    CategoryBaseObject success = new Gson().fromJson(json,CategoryBaseObject.class);
                    per_page = success.getProducts().getPer_page();
                    if (success.getProducts().getData().size()>0){
                        for (Product p : success.getProducts().getData()){
                            listOfProducts.add(p);
                            adapter.notifyDataSetChanged();
                        }
                        loadData.setVisibility(View.GONE);
                    }else {
                        Toast.makeText(CategoryProducts.this, "لا يوجد منتجات", Toast.LENGTH_SHORT).show();
                        loadData.setVisibility(View.GONE);
                    }
                }else {
                    Toast.makeText(CategoryProducts.this, "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                    loadData.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(CategoryProducts.this, "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
                loadData.setVisibility(View.GONE);
            }
        });
    }
    private void performPagination(int page){
        loadMore.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        SharedPreferences sharedPreferences1 = getSharedPreferences("Category",Context.MODE_PRIVATE);
        int category_id = sharedPreferences1.getInt("category_id",0);
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetCategoryProductsAPI getAllProductsAPI = retrofit.create(GetCategoryProductsAPI.class);
        Call<BaseResponse> call;
        call = getAllProductsAPI.getProducts(String.valueOf(category_id),page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    CategoryBaseObject success = new Gson().fromJson(json,CategoryBaseObject.class);
                    if (success.getProducts().getData().size()>0){
                        for (Product p : success.getProducts().getData()){
                            listOfProducts.add(p);
                            adapter.notifyDataSetChanged();
                        }

                    }else {
                        Toast.makeText(CategoryProducts.this, "لا يوجد منتجات إضافية", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(CategoryProducts.this, "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                }
                loadMore.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(CategoryProducts.this, "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
                loadMore.setVisibility(View.GONE);
            }
        });
    }
    public void performSortedPagination(String sort,int page){
        loadMore.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        SharedPreferences sharedPreferences1 = getSharedPreferences("Category",Context.MODE_PRIVATE);
        int category_id = sharedPreferences1.getInt("category_id",0);
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetSortedCategoryProductsAPI getAllProductsAPI = retrofit.create(GetSortedCategoryProductsAPI.class);
        Call<BaseResponse> call;
        call = getAllProductsAPI.getProducts(String.valueOf(category_id),sort,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    CategoryBaseObject success = new Gson().fromJson(json,CategoryBaseObject.class);
                    if (success.getProducts().getData().size()>0){
                        for (Product p : success.getProducts().getData()){
                            listOfProducts.add(p);
                            adapter.notifyDataSetChanged();
                        }

                    }else {
                        Toast.makeText(CategoryProducts.this, "لا يوجد منتجات إضافية", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(CategoryProducts.this, "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                }
                loadMore.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(CategoryProducts.this, "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
                loadMore.setVisibility(View.GONE);
            }
        });
    }
    public int convertSortTextToNum(String type){
        int output = 0;
        switch (type){
            case "by_date":output=0;break;
            case "by_price":output=1;break;
            case "by_name":output=2;break;
            case "by_average":output=3;break;
            case "by_visiting":output=4;break;
        }
        return output;
    }
    public void runAnimation(RecyclerView recyclerView,int type,OurProductsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
