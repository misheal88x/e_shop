package com.apps.scit.e_shop.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.Model.ConfigObject;
import com.apps.scit.e_shop.R;



public class About extends Fragment {
    TextView contactus;
    ImageButton facebook,email;
    Database database;
    String emailAddress = "";
    String mobileNumber = "";
    String facebookAddress = "";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_fragment,container,false);
        database = new Database(getContext());
        contactus = view.findViewById(R.id.about_contact_us);
        contactus.setPaintFlags(contactus.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        facebook = view.findViewById(R.id.about_facebook);
        email = view.findViewById(R.id.about_gmail);
        if (database.getConfigurations(0)!=null){
            if (database.getConfigurations(0).size()>0){
                for (ConfigObject co : database.getConfigurations(0)){
                    if (co.getName().equals("Email")){
                        emailAddress = co.getValue();
                        Log.i("config_value", "onCreateView: "+co.getValue());
                    }else if (co.getName().equals("Mobile")){
                        mobileNumber = co.getValue();
                        Log.i("config_value", "onCreateView: "+co.getValue());
                    }else if (co.getName().equals("FacebookLink")){
                        facebookAddress = co.getValue();
                        Log.i("config_value", "onCreateView: "+co.getValue());
                    }
                }
            }
        }
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
                String facebookUrl = getFacebookPageURL(getContext());
                facebookIntent.setData(Uri.parse(facebookUrl));
                startActivity(facebookIntent);
            }
        });
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_SEND);
                String[] recipients={emailAddress};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.setType("text/html");
                intent.setPackage("com.google.android.gm");
                startActivity(Intent.createChooser(intent, "Send mail"));
            }
        });
        return view;
    }
    public String getFacebookPageURL(Context context) {
        String facebookUrl = facebookAddress;
        String pageId = "EStore";
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + facebookUrl;
            } else { //older versions of fb app
                return "fb://page/" + pageId;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return facebookUrl; //normal web url
        }
    }
/*
    @Override
    public boolean onBackPressed() {
        return true;
    }
    **/
}
