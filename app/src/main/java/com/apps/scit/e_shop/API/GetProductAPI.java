package com.apps.scit.e_shop.API;

import com.apps.scit.e_shop.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Misheal on 7/29/2018.
 */

public interface GetProductAPI {
    @GET("api/v1/product/{product_id}")
    Call<BaseResponse> getProduct(@Path("product_id")String product_id
    );
}
