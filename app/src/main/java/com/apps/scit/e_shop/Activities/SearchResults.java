package com.apps.scit.e_shop.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.scit.e_shop.API.SearchAPI;
import com.apps.scit.e_shop.Adapters.CategoriesAdapter;
import com.apps.scit.e_shop.Adapters.MainSingleProductAdapter;
import com.apps.scit.e_shop.Adapters.MostSearchedCategoriesAdapter;
import com.apps.scit.e_shop.Adapters.OffersAdapter;
import com.apps.scit.e_shop.Adapters.OurProductsAdapter;
import com.apps.scit.e_shop.Adapters.RelatedProductsAdapter;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.Extra.EndlessRecyclerViewScrollListener;
import com.apps.scit.e_shop.Model.BaseResponse;
import com.apps.scit.e_shop.Model.Category;
import com.apps.scit.e_shop.Model.Offer;
import com.apps.scit.e_shop.Model.Product;
import com.apps.scit.e_shop.Model.SearchResultObject;
import com.apps.scit.e_shop.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchResults extends AppCompatActivity {
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
Toolbar toolbar;
TextView toolbarText,specialOffersText,normalOffersText,categoriesText,productsText,message;
ViewPager specialOffersViewPager;
RecyclerView normalOffersRecycler,categoriesRecycler,productsRecycler;
List<Offer> listOfSpecialOffers;
List<Category> listOfCateories;
List<Product> listOfProducts,listOfNormalOffers;
ProgressBar loadData,loadMoreProducts;
NestedScrollView scrollView;
OffersAdapter specialOffersAdapter;
OurProductsAdapter productsAdapter;
MainSingleProductAdapter normalOffersAdapter;
    //Normal Offers Pagination Variables
    private int NOpageNumber = 1;
    private int per_page = 20;
    private int itemCount = 10;
    private Boolean NOisLoading = true;
    private int NOpastVisibleItems,NOvisibleItemCount,NOtotalItemCount,NOpreviousTotal = 0;
    private int NOviewThreshold = 10;
    //Products Pagination Variables
    private int PpageNumber = 1;
    private Boolean PisLoading = true;
    private int PpastVisibleItems,PvisibleItemCount,PtotalItemCount,PpreviousTotal = 0;
    private int PviewThreshold = 10;
private static String BASE_URL;
Database database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);
        if (getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_LTR){
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
        definingIDs();
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbarText.setText("نتائج البحث");
        getSupportActionBar().setTitle("");
        listOfSpecialOffers = new ArrayList<>();
        listOfCateories = new ArrayList<>();
        listOfProducts = new ArrayList<>();
        listOfNormalOffers = new ArrayList<>();
        hideAllElements();
        Intent intent = getIntent();
        final String text = intent.getStringExtra("searchText");
        final int productFlag = intent.getIntExtra("products",0);
        final int categoryFlag = intent.getIntExtra("categories",0);
        final int offerFlag = intent.getIntExtra("offers",0);
        final String sort = intent.getStringExtra("sortBy");
        callAPI(text,productFlag,categoryFlag,offerFlag,sort);
        //LayoutManagers
        final LinearLayoutManager normalOffersLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        RecyclerView.LayoutManager categoriesLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        final GridLayoutManager productsLayoutManager = new GridLayoutManager(this,2);
        //Adapters
        specialOffersAdapter = new OffersAdapter(SearchResults.this,listOfSpecialOffers);
        MostSearchedCategoriesAdapter categoriesAdapter = new MostSearchedCategoriesAdapter(this,listOfCateories);
        normalOffersAdapter = new MainSingleProductAdapter(this,listOfNormalOffers);
        productsAdapter = new OurProductsAdapter(this,listOfProducts);
        //Set LayoutManagers
        normalOffersRecycler.setLayoutManager(normalOffersLayoutManager);
        categoriesRecycler.setLayoutManager(categoriesLayoutManager);
        productsRecycler.setLayoutManager(productsLayoutManager);

        //Set Adapters
        specialOffersViewPager.setAdapter(specialOffersAdapter);
        runAnimationOffers(normalOffersRecycler,0,normalOffersAdapter);
        //normalOffersRecycler.setAdapter(normalOffersAdapter);
        runAnimationCategories(categoriesRecycler,0,categoriesAdapter);
        //categoriesRecycler.setAdapter(categoriesAdapter);
        runAnimationProducts(productsRecycler,0,productsAdapter);
        //productsRecycler.setAdapter(productsAdapter);
        normalOffersRecycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(normalOffersLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (listOfProducts.size()>=per_page){
                    NOpageNumber++;
                    performProductPagination(NOpageNumber,text,productFlag,categoryFlag,offerFlag,sort);
                }
            }
        });
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        PpageNumber++;
                        performProductPagination(PpageNumber,text,productFlag,categoryFlag,offerFlag,sort);
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_results_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return true;
    }
    public void definingIDs(){
        //Base url
        BASE_URL = getResources().getString(R.string.base_url);
        //Toolbar
        toolbar = findViewById(R.id.search_results_toolbar);
        toolbarText = toolbar.findViewById(R.id.basicToolbarTitle);
        //TextViews
        specialOffersText = findViewById(R.id.search_results_special_offers_text);
        normalOffersText = findViewById(R.id.search_results_normal_offers_text);
        categoriesText = findViewById(R.id.search_results_categories_text);
        productsText = findViewById(R.id.search_results_products_text);
        message = findViewById(R.id.search_results_message);
        //ViewPager
        specialOffersViewPager = findViewById(R.id.search_results_special_offers_viewpager);
        //RecyclerView
        normalOffersRecycler = findViewById(R.id.search_results_normal_offers_recycler);
        categoriesRecycler = findViewById(R.id.search_results_categories_recycler);
        productsRecycler = findViewById(R.id.search_results_products_recycler);
        //ProgressBar
        loadData = findViewById(R.id.search_results_load_data);
        loadMoreProducts = findViewById(R.id.search_results_load_more_products);
        //ScrollView
        scrollView = findViewById(R.id.search_results_scrollview);
        database = new Database(this);
    }
    private void callAPI(String text,int productFlag,int categoryFlag,int offerFlag,String sort) {
        message.setVisibility(View.GONE);
        SharedPreferences sharedPreferences = getSharedPreferences("Profile", Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS).addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        SearchAPI api = retrofit.create(SearchAPI.class);
        Call<BaseResponse> call;
        call = api.search(1,text,sort,categoryFlag,productFlag,offerFlag);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108) {
                    String json = new Gson().toJson(response.body().getData());
                    SearchResultObject success = new Gson().fromJson(json, SearchResultObject.class);
                    message.setVisibility(View.VISIBLE);
                    showAllElements();
                    //Special Offers
                    listOfSpecialOffers = new ArrayList<>();
                    if (success.getSpecial_offers().size() > 0) {
                        for (Offer o : success.getSpecial_offers()) {
                            listOfSpecialOffers.add(o);
                        }
                        specialOffersAdapter = new OffersAdapter(SearchResults.this,listOfSpecialOffers);
                        specialOffersViewPager.setAdapter(specialOffersAdapter);
                        message.setVisibility(View.GONE);
                    } else {
                        hideSpecialOffersSection();
                    }

                    //Normal Offers
                    if (success.getNormal_offers()!=null){
                        if (success.getNormal_offers().getPer_page()!=0){
                            per_page = success.getNormal_offers().getPer_page();
                        }
                        if (success.getNormal_offers().getData().size() > 0) {
                            for (Product no : success.getNormal_offers().getData()) {
                                listOfNormalOffers.add(no);
                            }
                            message.setVisibility(View.GONE);
                        } else {
                            hideNormalOffersSection();
                        }
                    }else {
                        hideNormalOffersSection();
                    }


                    //Categories
                    if (success.getCategories().size() > 0) {
                        for (Category c : success.getCategories()) {
                            listOfCateories.add(c);
                        }
                        message.setVisibility(View.GONE);
                    } else {
                        hideCategoriesSection();
                    }

                    //Products
                    if (success.getProducts()!=null){
                        if (success.getProducts().getData().size() > 0) {
                            for (Product p : success.getProducts().getData()) {
                                listOfProducts.add(p);
                            }
                            message.setVisibility(View.GONE);
                        } else {
                            hideProductsSection();
                        }
                    }else {
                        hideProductsSection();
                    }
                }else {
                    message.setVisibility(View.VISIBLE);
                    loadData.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                message.setVisibility(View.VISIBLE);
                loadData.setVisibility(View.GONE);
                Toast.makeText(SearchResults.this, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void hideAllElements(){
        scrollView.setVisibility(View.GONE);
        loadData.setVisibility(View.VISIBLE);
    }
    public void showAllElements(){
        scrollView.setVisibility(View.VISIBLE);
        loadData.setVisibility(View.GONE);
    }
    public void hideSpecialOffersSection(){
        specialOffersText.setVisibility(View.GONE);
        specialOffersViewPager.setVisibility(View.GONE);
    }
    public void showSpecialOffersSection(){
        specialOffersText.setVisibility(View.VISIBLE);
        specialOffersViewPager.setVisibility(View.VISIBLE);
    }
    public void hideNormalOffersSection(){
        normalOffersText.setVisibility(View.GONE);
        normalOffersRecycler.setVisibility(View.GONE);
    }
    public void showNormalOffersSection(){
        normalOffersText.setVisibility(View.VISIBLE);
        normalOffersRecycler.setVisibility(View.VISIBLE);
    }
    public void hideCategoriesSection(){
        categoriesText.setVisibility(View.GONE);
        categoriesRecycler.setVisibility(View.GONE);
    }
    public void showCategoriesSection(){
        categoriesText.setVisibility(View.VISIBLE);
        categoriesRecycler.setVisibility(View.VISIBLE);
    }
    public void hideProductsSection(){
        productsText.setVisibility(View.GONE);
        productsRecycler.setVisibility(View.GONE);
    }
    public void showProductsSection(){
        productsText.setVisibility(View.VISIBLE);
        productsRecycler.setVisibility(View.VISIBLE);
    }
    public void performProductPagination(int page,String text,int productFlag,int categoryFlag,int offerFlag,String sort){
        loadMoreProducts.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        SearchAPI api = retrofit.create(SearchAPI.class);
        Call<BaseResponse> call;
        call = api.search(page,text,sort,categoryFlag,productFlag,offerFlag);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    SearchResultObject success =  new Gson().fromJson(json,SearchResultObject.class);

                    if (success.getProducts()!=null){
                        if (success.getProducts().getData().size()>0){
                            for (Product p : success.getProducts().getData()){
                                listOfProducts.add(p);
                                productsAdapter.notifyDataSetChanged();
                            }
                        }else {
                            Toast.makeText(SearchResults.this, "لا يوجد منتجات إضافية", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else {
                    Toast.makeText(SearchResults.this, "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                }
                loadMoreProducts.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(SearchResults.this, "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void performNormalOffersPagination(int page,String text,int productFlag,int categoryFlag,int offerFlag,String sort){
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        SearchAPI api = retrofit.create(SearchAPI.class);
        Call<BaseResponse> call;
        call = api.search(page,text,sort,categoryFlag,productFlag,offerFlag);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    SearchResultObject success =  new Gson().fromJson(json,SearchResultObject.class);

                    if (success.getNormal_offers()!=null){
                        if (success.getNormal_offers().getData().size()>0){
                            for (Product p : success.getNormal_offers().getData()){
                                listOfNormalOffers.add(p);
                                normalOffersAdapter.notifyDataSetChanged();
                            }
                        }else {
                            Toast.makeText(SearchResults.this, "لا يوجد عروض إضافية", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else {
                    Toast.makeText(SearchResults.this, "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(SearchResults.this, "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void runAnimationCategories(RecyclerView recyclerView,int type,MostSearchedCategoriesAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
    public void runAnimationOffers(RecyclerView recyclerView,int type,MainSingleProductAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
    public void runAnimationProducts(RecyclerView recyclerView,int type,OurProductsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
