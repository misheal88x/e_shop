package com.apps.scit.e_shop.Adapters;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.scit.e_shop.Activities.ProductDetails;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.MainActivity;
import com.apps.scit.e_shop.Model.Product;
import com.apps.scit.e_shop.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * Created by Misheal on 6/23/2018.
 */

public class RelatedProductsAdapter extends RecyclerView.Adapter<RelatedProductsAdapter.MyViewHolder> {
    private Context context;
    private List<Product> listOfProducts;
    private CategoriesAdapter.OnItemClickListenter mListener;
    private Database database;
    public interface OnItemClickListenter{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(CategoriesAdapter.OnItemClickListenter listener){
        mListener = listener;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView productName;
        public TextView productPrice;
        public ImageView productImage;
        public RatingBar productRate;
        public RelativeLayout layout;
        public MyViewHolder(View view, final CategoriesAdapter.OnItemClickListenter listenter){
            super(view);
            productName = view.findViewById(R.id.related_product_name);
            productPrice = view.findViewById(R.id.related_product_price);
            productRate = view.findViewById(R.id.related_product_rate);
            productImage = view.findViewById(R.id.related_product_image);
            layout =view.findViewById(R.id.related_product_layout);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (!(isOnline())&&database.getSingleProduct(listOfProducts.get(position).getId()).getId()==0){
                        Toast.makeText(context, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
                    }else {
                        if (listenter!=null){
                            if (position!=RecyclerView.NO_POSITION){
                                listenter.onItemClick(position);
                            }
                        }
                        else {
                            Product product = listOfProducts.get(position);
                            SharedPreferences sharedPreferences = context.getSharedPreferences("product",Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("productType","product");
                            editor.putInt("product_id",product.getId());
                            editor.commit();
                            Intent intent = new Intent(context,ProductDetails.class);
                            intent.putExtra("product_id",String.valueOf(product.getId()));
                            intent.putExtra("productType","product");
                            context.startActivity(intent);
                        }
                    }

                }
            });
        }
    }
    public RelatedProductsAdapter(Context context, List<Product> listOfProducts){
        this.context = context;
        this.listOfProducts = listOfProducts;
        this.database = new Database(context);
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.related_product_card,parent,false);
        return new MyViewHolder(itemView,mListener);
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Product product = listOfProducts.get(position);
        holder.productName.setText(product.getName());
        holder.productPrice.setText(String.valueOf(product.getPrice()));
        try {
            holder.productRate.setRating(Float.parseFloat(product.getAvgRate()));
        }catch (Exception e){}

        if (product.getImage()!=null){
            GlideApp.with(context).load(product.getImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.offline).centerCrop().into(holder.productImage);
            //Picasso.with(context).load(product.getImage())
                    //.placeholder(R.drawable.offline).into(holder.productImage);
        }

        /*
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Below", Toast.LENGTH_SHORT).show();
                SharedPreferences sharedPreferences = context.getSharedPreferences("product",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("productType","product");
                editor.putInt("product_id",product.getId());
                editor.commit();
                Intent intent = new Intent(context,ProductDetails.class);
                context.startActivity(intent);
            }
        });
        **/

    }
    @Override
    public int getItemCount() {
        return listOfProducts.size();
    }
    public boolean isOnline(){
        ConnectivityManager conMgr =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null){
            return false;
        }else {
            return true;
        }
    }
}
