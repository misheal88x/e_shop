package com.apps.scit.e_shop.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.scit.e_shop.Activities.ProductDetails;
import com.apps.scit.e_shop.Model.Product;
import com.apps.scit.e_shop.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;

import java.util.List;

/**
 * Created by Misheal on 6/26/2018.
 */

public class OfferProductsAdapter extends RecyclerView.Adapter<OfferProductsAdapter.MyViewHolder> {
    private Context context;
    private List<Product> listOfProducts;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView productName;
        public TextView productOldPrice;
        public TextView productNewPrice;
        public ImageView productImage;
        public RatingBar productRate;
        public TextView remainingTime;
        public RelativeLayout layout;
        public MyViewHolder(View view){
            super(view);
            productName = view.findViewById(R.id.offer_product_name);
            productOldPrice = view.findViewById(R.id.offer_product_old_price);
            productNewPrice = view.findViewById(R.id.offer_product_new_price);
            productImage = view.findViewById(R.id.offer_product_image);
            productRate = view.findViewById(R.id.offer_product_rate);
            remainingTime = view.findViewById(R.id.normal_offer_remaining_time2);
            remainingTime.bringToFront();
            layout = view.findViewById(R.id.offer_product_layout);
        }
    }
    public OfferProductsAdapter(Context context, List<Product> listOfProducts){
        this.context = context;
        this.listOfProducts = listOfProducts;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_product_card,parent,false);
        return new MyViewHolder(itemView);
    }

    private Picasso getCustomPicasso(){
        Picasso.Builder builder = new Picasso.Builder(context);
        //set request transformer
        Picasso.RequestTransformer requestTransformer =  new Picasso.RequestTransformer() {
            @Override
            public Request transformRequest(Request request) {
                Log.d("image request", request.toString());
                return request;
            }
        };
        builder.requestTransformer(requestTransformer);
        return builder.build();
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Product product = listOfProducts.get(position);
        holder.productName.setText(product.getName());
        holder.productOldPrice.setText(String.valueOf(product.getPrice()));
        holder.productNewPrice.setText(String.valueOf(product.getOffer_price()));
        holder.productRate.setRating(Float.parseFloat(product.getAvgRate()));
        if (product.getImage()!=null){
            //Picasso.setSingletonInstance(getCustomPicasso());
            GlideApp.with(context).load(product.getImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.offline).centerCrop().into(holder.productImage);
            //Picasso.with(context).load(product.getImage()).placeholder(R.drawable.offline).into(holder.productImage);
        }
        if (product.getOffer_remain() == 0){
            holder.remainingTime.setText("00:00:00");
        }else {
            holder.remainingTime.setText(formatSeconds(Long.parseLong(String.valueOf(product.getOffer_remain()))));
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("product",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("productType","offer");
                editor.putInt("product_id",product.getId());
                editor.commit();
                Intent intent = new Intent(context,ProductDetails.class);
                intent.putExtra("product_id",String.valueOf(product.getId()));
                intent.putExtra("productType","offer");
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return listOfProducts.size();
    }
    public  String formatSeconds(long totalSecs){
        long days = (int)(totalSecs/86400);
        long hours =  (int)((totalSecs % 86400) / 3600);
        long minutes =  (int)((totalSecs % 3600) / 60);
        long seconds =  (int)(totalSecs % 60);
        if (days==0){
            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        }else {
            return String.format("%02d:%02d:%02d", hours, minutes, seconds)+" و "+String.valueOf(days)+" يوم";
        }

    }
}
