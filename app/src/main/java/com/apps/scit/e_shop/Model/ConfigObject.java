package com.apps.scit.e_shop.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 7/10/2018.
 */

public class ConfigObject {
    //@SerializedName("id") private int id = 0;
    //@SerializedName("config_id") private int config_id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("value") private String value = "";

    public ConfigObject(int id, int config_id, String name, String value) {
        //this.id = id;
        //this.config_id = config_id;
        this.name = name;
        this.value = value;
    }

    /*
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getConfig_id() {
        return config_id;
    }

    public void setConfig_id(int config_id) {
        this.config_id = config_id;
    }

    **/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
