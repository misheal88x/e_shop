package com.apps.scit.e_shop.Model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Offer {
    @SerializedName("id") private int id = 0;
    @SerializedName("shop_id") private int shop_id = 0;
    @SerializedName("information") private String information = "";
    @SerializedName("full_image_url") private String full_image_url = "";
    @SerializedName("start_date") private String start_date = "";
    @SerializedName("end_date") private String end_date = "";
    @SerializedName("products") private List<Product> products = new ArrayList<>();
    @SerializedName("offer_price") private int offer_price = 0;
    @SerializedName("price") private int price = 0;
    @SerializedName("offer_remain") private String offer_remain = "";
    @SerializedName("images") private List<String> images = new ArrayList<>();


    public Offer( String end_date) {
        this.end_date = end_date;
    }
    public Offer(){}
    public Offer(int id, String information, String image_url, String start_date, String end_date, int offer_price, int price,String offer_remain) {
        this.id = id;
        this.information = information;
        this.full_image_url = image_url;
        this.start_date = start_date;
        this.end_date = end_date;
        this.offer_price = offer_price;
        this.price = price;
        this.offer_remain = offer_remain;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInformation() {
        return information;
    }

    public String getImage_url() {
        return full_image_url;
    }

    public void setImage_url(String full_image_url) {
        this.full_image_url = full_image_url;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getOffer_price() {
        return offer_price;
    }

    public void setOffer_price(int offer_price) {
        this.offer_price = offer_price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getOffer_remain() {
        return offer_remain;
    }

    public void setOffer_remain(String offer_remain) {
        this.offer_remain = offer_remain;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }
}
