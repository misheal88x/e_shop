package com.apps.scit.e_shop.API;

import com.apps.scit.e_shop.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Misheal on 7/28/2018.
 */

public interface GetMessagesAPI {
    @FormUrlEncoded
    @POST("api/v1/visitor/get_messages")
    Call<BaseResponse> getMessages(@Field("platform")String paltform);
}
