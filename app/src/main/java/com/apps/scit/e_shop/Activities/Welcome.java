package com.apps.scit.e_shop.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.apps.scit.e_shop.API.CreateAccoutAPI;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.MainActivity;
import com.apps.scit.e_shop.Model.BaseResponse;
import com.apps.scit.e_shop.Model.Category;
import com.apps.scit.e_shop.Model.ConfigMainObject;
import com.apps.scit.e_shop.Model.ConfigObject;
import com.apps.scit.e_shop.Model.Offer;
import com.apps.scit.e_shop.Model.Product;
import com.apps.scit.e_shop.Model.ProfileObject;
import com.apps.scit.e_shop.Model.SliderObject;
import com.apps.scit.e_shop.Model.SpecialOfferImage;
import com.apps.scit.e_shop.Model.SuccessCreateAccount;
import com.apps.scit.e_shop.R;
import com.google.gson.Gson;
import com.kosalgeek.android.md5simply.MD5;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Welcome extends AppCompatActivity {
    private static String BASE_URL;
    private static int SPLASH_TIME_OUT=0;
    AVLoadingIndicatorView indicator;
    Boolean createAccountCalled = false;
    Database database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_welcome);
        database = new Database(Welcome.this);
        indicator = findViewById(R.id.splash_indicator);
        indicator.show();
        //Base url
        BASE_URL = getResources().getString(R.string.base_url);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isOnline()){
                    if (!database.getProfile().getToken().equals("")){
                        Intent intent = new Intent(Welcome.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(Welcome.this);
                        builder.setTitle("غير مسجل").setMessage("لا يوجد اتصال بالانترنت و لست مسجلا بالتطبيق, الرجاء الاتصال بالانترنت ثم أعد المحاولة, سيتم إغلاق التطبيق").setPositiveButton("حسنا", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }).show();
                    }
                }else {
                    createAccountCall();
                }

            }
        },SPLASH_TIME_OUT);
    }
    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }
    public static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] textBytes = text.getBytes("iso-8859-1");
        md.update(textBytes, 0, textBytes.length);
        byte[] sha1hash = md.digest();
        return convertToHex(sha1hash);
    }
    public boolean isOnline(){
        ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null){
            return false;
        }else {
            return true;
        }
    }

    public void createAccountCall(){
        createAccountCalled = true;
        String android_id = Settings.Secure.getString(Welcome.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        try {
            String signature = MD5.encrypt(SHA1(SHA1(MD5.encrypt(android_id))));
            Log.i("signiture", "createAccountCall: "+signature);
            Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
            CreateAccoutAPI createAccoutService = retrofit.create(CreateAccoutAPI.class);
            Call<BaseResponse> call;
            //todo add shop_token
            call = createAccoutService.create("046d5ff64b37f35ccc462e73565e170b12",android_id,signature,"android");
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.body().getCode()==200&&response.body().getMessage_code()==112) {
                        Toast.makeText(Welcome.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                        String json = new Gson().toJson(response.body().getData());
                        SuccessCreateAccount success = new Gson().fromJson(json,SuccessCreateAccount.class);
                        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("token",success.getProfile().getToken());
                        editor.commit();
                        clearDB();
                        insertToDB(success);
                        Intent intent = new Intent(Welcome.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else if (response.body().getCode()==200&&response.body().getMessage_code()==107){
                        String json = new Gson().toJson(response.body().getData());
                        SuccessCreateAccount success = new Gson().fromJson(json,SuccessCreateAccount.class);
                        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("token",success.getProfile().getToken());
                        editor.commit();
                        clearDB();
                        insertToDB(success);
                        Intent intent = new Intent(Welcome.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    if (!database.getProfile().getToken().equals("")){
                        Intent intent = new Intent(Welcome.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(Welcome.this);
                        builder.setTitle("غير مسجل").setMessage("لا يوجد اتصال بالانترنت و لست مسجلا بالتطبيق, الرجاء الاتصال بالانترنت ثم أعد المحاولة, سيتم إغلاق التطبيق").setPositiveButton("حسنا", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }).show();
                    }
                }
            });
           /*
            RestAdapter createAccoutAdapter = new RestAdapter.Builder().setEndpoint(BASE_URL).build();
            CreateAccoutAPI createAccoutService = createAccoutAdapter.create(CreateAccoutAPI.class);
            createAccoutService.create(android_id, signature, "android", new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                    if (baseResponse.getCode()==200&&baseResponse.getMessage_code()==112) {
                        Toast.makeText(Welcome.this, baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (baseResponse.getCode()==200&&baseResponse.getMessage_code()==108){
                        String json = new Gson().toJson(baseResponse.getData());
                        SuccessCreateAccount success = new Gson().fromJson(json,SuccessCreateAccount.class);
                        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("token",success.getProfile().getToken());
                        editor.commit();
                        clearDB();
                        insertToDB(success);
                        Intent intent = new Intent(Welcome.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else if (baseResponse.getCode()==200&&baseResponse.getMessage_code()==107){
                        String json = new Gson().toJson(baseResponse.getData());
                        SuccessCreateAccount success = new Gson().fromJson(json,SuccessCreateAccount.class);
                        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("token",success.getProfile().getToken());
                        editor.commit();
                        clearDB();
                        insertToDB(success);
                        Intent intent = new Intent(Welcome.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (!database.getProfile().getToken().equals("")){
                        Intent intent = new Intent(Welcome.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(Welcome.this);
                        builder.setTitle("غير مسجل").setMessage("لا يوجد اتصال بالانترنت و لست مسجلا بالتطبيق, الرجاء الاتصال بالانترنت ثم أعد المحاولة, سيتم إغلاق التطبيق").setPositiveButton("حسنا", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }).show();
                    }
                }
            });
            **/
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void insertToDB(SuccessCreateAccount success){
        //Configs
        if (success.getConfigs().size()>0){
            for (ConfigMainObject c : success.getConfigs()){
                database.insertConfig(c.getId(),c.getName(),c.getUnqiue_name());
                //Configurations
                if (c.getConfigurations().size()>0){
                    for (ConfigObject o : c.getConfigurations()){
                        database.insertConfiguration(0,0,o.getName(),o.getValue());
                    }
                }
            }
        }

      //Slider
        if (success.getSlider().size()>0){
            int j = 0;
            for (SliderObject s : success.getSlider()){
                if (s.getTitle()==null){
                    database.insertSlider(j,"title"+String.valueOf(j),s.getImage(),s.getTarget(),s.getUrl());
                }else {
                    database.insertSlider(j,s.getTitle(),s.getImage(),s.getTarget(),s.getUrl());
                }
                j++;
            }
        }

        //Categories
        if (success.getCategories().size()>0){
            for (Category c : success.getCategories()){
                database.insertCategory(c.getId(),c.getName(),c.getImage(),c.getIcon_image(),0);
                if (c.getSub_categories().size()>0){
                    insertSubCategories(c);
                }
            }
        }

        //Products
        if (success.getProducts().size()>0){
            for (Product p : success.getProducts()){
            database.insertProduct(p.getId(),p.getName(),p.getImage(),p.getDescription(),p.getPrice(),p.getAvgRate()
                ,p.getOffer_price(),p.getIs_offer(),p.getCategory_name());
            }
        }


        //Normal Offers
        if (success.getNormal_offers().size()>0){
            for (Product p : success.getNormal_offers()){
            database.insertNormalOffer(p.getId(),p.getName(),p.getImage(),p.getDescription(),p.getPrice(),
                        p.getCategory_name(),p.getAvgRate(),p.getOffer_price(),p.getIs_offer(),String.valueOf(p.getOffer_remain()));
            }
        }

        //Special Offers
        if (success.getSpecial_offers().size()>0){
            for (Offer o : success.getSpecial_offers()){
                database.insertSpecialOffer(o.getId(),o.getInformation(),o.getImage_url(),o.getStart_date(),o.getEnd_date(),
                        o.getOffer_price(),o.getPrice(),o.getOffer_remain());
                //Special Offer Products
                if (o.getProducts().size()>0){
                    for (Product p : o.getProducts()){
                        database.insertSpecialOfferProduct(p.getId(),p.getName(),p.getImage(),p.getDescription(),p.getPrice()
                        ,p.getCategory_name(),p.getIs_offer(),p.getOffer_price(),p.getAvgRate(),o.getId());
                    }
                }
                //Special Offer Images
                if (o.getImages().size()>0){
                    int j = 0;
                    for (String i : o.getImages()){
                        database.insertSpecialOfferImage(j,o.getId(),i);
                        j++;
                    }
                }
            }
        }

        //Top Searched Products
        if (success.getTop_searched_products().size()>0){
            for (Product p : success.getTop_searched_products()) {
                if (database.getSingleProduct(p.getId()).getId()==0){
                    database.insertProduct(p.getId(),p.getName(),p.getImage(),p.getDescription(),p.getPrice(),p.getAvgRate(),p.getOffer_price(),
                            p.getIs_offer(),p.getCategory_name());
                }
               database.insertTopSearchedProduct(p.getId(), p.getName(), p.getImage(), p.getDescription(), p.getPrice(), p.getCategory_name(),
                        p.getAvgRate(), p.getOffer_price(), p.getIs_offer());
            }
        }


        //Top Rated Products
        if (success.getTop_rated_products().size()>0){
            for (Product p : success.getTop_rated_products()){
                if (database.getSingleProduct(p.getId()).getId()==0){
                    database.insertProduct(p.getId(),p.getName(),p.getImage(),p.getDescription(),p.getPrice(),p.getAvgRate(),p.getOffer_price(),
                            p.getIs_offer(),p.getCategory_name());
                }
                database.insertTopRatedProduct(p.getId(),p.getName(),p.getImage(),p.getDescription(),p.getPrice(),
                        p.getCategory_name(),p.getAvgRate(),p.getOffer_price(),p.getIs_offer());
            }
        }


        //Top Viewed Products
        if (success.getTop_viewed_products().size()>0){
            for (Product p : success.getTop_viewed_products()){
                if (database.getSingleProduct(p.getId()).getId()==0){
                    database.insertProduct(p.getId(),p.getName(),p.getImage(),p.getDescription(),p.getPrice(),p.getAvgRate(),p.getOffer_price(),
                            p.getIs_offer(),p.getCategory_name());
                }
                database.insertTopViewedProduct(p.getId(),p.getName(),p.getImage(),p.getDescription(),
                        p.getPrice(),p.getCategory_name(),p.getAvgRate(),p.getOffer_price(),p.getIs_offer());
            }
        }

        //Profile
        if (!success.getProfile().getToken().equals("")){
            ProfileObject profileObject = success.getProfile();
            long profi = database.insertProfile(profileObject.getToken(),profileObject.getProduct_notification_flag(),
                    profileObject.getOffer_notification_flag(),profileObject.getMobile_number(),profileObject.getId());

        }
    }

    public void insertSubCategories(Category category){
        for (Category ca : category.getSub_categories()){
            database.insertCategory(ca.getId(),ca.getName(),ca.getImage(),ca.getIcon_image(),0);
            if (ca.getSub_categories().size()>0){
                insertSubCategories(ca);
            }
        }

    }
    public void clearDB(){
        database.clearConfigs();
        database.clearConfigurations();
        database.clearSlider();
        database.clearCategories();
        database.clearProducts();
        database.clearNormalOffers();
        database.clearSpecialOffers();
        database.clearSpecialOfferProducts();
        database.clearSpecialOfferImages();
        database.clearTopRatedProducts();
        database.clearTopSearchedProducts();
        database.clearTopViewedProducts();
        database.clearProfile();
    }
}
