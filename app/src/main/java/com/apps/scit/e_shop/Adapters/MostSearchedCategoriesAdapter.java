package com.apps.scit.e_shop.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.scit.e_shop.Activities.AllCategories;
import com.apps.scit.e_shop.Activities.CategoryProducts;
import com.apps.scit.e_shop.Database;
import com.apps.scit.e_shop.Model.Category;
import com.apps.scit.e_shop.R;

import java.util.List;
import java.util.Random;

/**
 * Created by Misheal on 6/24/2018.
 */

public class MostSearchedCategoriesAdapter extends RecyclerView.Adapter<MostSearchedCategoriesAdapter.MyViewHolder> {
    private Context context;
    private List<Category> listOfCategories;
    private Database database;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView categoryName;
        public MyViewHolder(View view){
            super(view);
            categoryName = view.findViewById(R.id.most_searched_categories_name);
        }
    }
    public MostSearchedCategoriesAdapter(Context context, List<Category> listOfCategories){
        this.context = context;
        this.listOfCategories = listOfCategories;
        this.database = new Database(context);
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.most_searched_categories_card,parent,false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Category category = listOfCategories.get(position);
        holder.categoryName.setText(category.getName());
        switch (position){
            case 0:{
                holder.categoryName.setBackgroundColor(context.getResources().getColor(R.color.green2));
                holder.categoryName.setTextColor(context.getResources().getColor(android.R.color.black));
                break;
            }
            case 1:{
                holder.categoryName.setBackgroundColor(context.getResources().getColor(android.R.color.black));
                holder.categoryName.setTextColor(context.getResources().getColor(android.R.color.white));
                break;
            }
            case 2:{
                holder.categoryName.setBackgroundColor(context.getResources().getColor(android.R.color.white));
                holder.categoryName.setTextColor(context.getResources().getColor(android.R.color.black));
                break;
            }
            case 3:{
                holder.categoryName.setBackgroundColor(context.getResources().getColor(R.color.green7));
                holder.categoryName.setTextColor(context.getResources().getColor(android.R.color.black));
                break;
            }
            case 4:{
                holder.categoryName.setBackgroundColor(context.getResources().getColor(android.R.color.black));
                holder.categoryName.setTextColor(context.getResources().getColor(android.R.color.white));
                break;
            }default:{
                Random randrom = new Random();
                int r = randrom.nextInt(100)+1;
                if (r>=1&&r<=25){
                    holder.categoryName.setBackgroundColor(context.getResources().getColor(R.color.green2));
                    holder.categoryName.setTextColor(context.getResources().getColor(android.R.color.black));
                }else if (r>=26&&r<=50){
                    holder.categoryName.setBackgroundColor(context.getResources().getColor(android.R.color.black));
                    holder.categoryName.setTextColor(context.getResources().getColor(android.R.color.white));
                }else if (r>=51&&r<=75){
                    holder.categoryName.setBackgroundColor(context.getResources().getColor(android.R.color.white));
                    holder.categoryName.setTextColor(context.getResources().getColor(android.R.color.black));
                }else{
                    holder.categoryName.setBackgroundColor(context.getResources().getColor(R.color.green7));
                    holder.categoryName.setTextColor(context.getResources().getColor(android.R.color.black));
                }
            }
        }
        if (listOfCategories.get(position).getName().equals("الكل")){
            holder.categoryName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, AllCategories.class);
                    context.startActivity(intent);
                }
            });
        }else {
            holder.categoryName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (database.getCategories(listOfCategories.get(position).getId()).size()>0){
                        SharedPreferences sharedPreferences = context.getSharedPreferences("Category",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("category_id",listOfCategories.get(position).getId());
                        editor.putString("category_name",listOfCategories.get(position).getName());
                        editor.putBoolean("FromMain",true);
                        editor.commit();
                        Intent intent = new Intent(context, AllCategories.class);
                        context.startActivity(intent);
                    }else {
                        SharedPreferences sharedPreferences = context.getSharedPreferences("Category",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("category_id",listOfCategories.get(position).getId());
                        editor.putString("category_name",listOfCategories.get(position).getName());
                        editor.commit();
                        Intent intent = new Intent(context,CategoryProducts.class);
                        context.startActivity(intent);
                    }

                }
            });
        }

    }
    @Override
    public int getItemCount() {
        return listOfCategories.size();
    }
}
