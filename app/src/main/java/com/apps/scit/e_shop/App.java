package com.apps.scit.e_shop;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.widget.Toast;

import org.acra.ACRA;
import org.acra.annotation.AcraCore;
import org.acra.annotation.AcraHttpSender;
import org.acra.annotation.AcraToast;
import org.acra.data.StringFormat;
import org.acra.sender.HttpSender;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Misheal on 7/16/2018.
 */
@AcraToast(resText=R.string.acra_toast_text,
        length = Toast.LENGTH_LONG)
@AcraCore(reportFormat= StringFormat.KEY_VALUE_LIST)
//@AcraMailSender(mailTo = "d.hasan@scit.co")
@AcraHttpSender(uri = "http://e-shop.scit.co/crash_reporter/save_log.php",
        httpMethod = HttpSender.Method.POST)
public class App extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/STC-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        // The following line triggers the initialization of ACRA
        ACRA.init(this);
    }
}
