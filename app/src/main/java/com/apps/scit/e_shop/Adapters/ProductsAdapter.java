package com.apps.scit.e_shop.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.scit.e_shop.Activities.ProductDetails;
import com.apps.scit.e_shop.Model.Product;
import com.apps.scit.e_shop.R;

import java.util.List;



public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {
    private Context context;
    private List<Product> listOfProducts;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView productName;
        public RatingBar productRate;
        public TextView productPrice;
        public ImageView productImage;
        public TextView productDescribtion;
        public RelativeLayout productLayout;
        public MyViewHolder(View view){
            super(view);
            productName = view.findViewById(R.id.product_name);
            productRate = view.findViewById(R.id.product_rate);
            productPrice = view.findViewById(R.id.product_price);
            productImage = view.findViewById(R.id.product_image);
            productDescribtion = view.findViewById(R.id.product_describtion);
            productLayout = view.findViewById(R.id.product_row_layout);
        }
    }
    public ProductsAdapter(Context context, List<Product> listOfProducts){
        this.context = context;
        this.listOfProducts = listOfProducts;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_card,parent,false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Product product = listOfProducts.get(position);
        holder.productName.setText(product.getName());
        holder.productRate.setRating(5);
        holder.productPrice.setText(String.valueOf(product.getPrice()));
        holder.productDescribtion.setText(product.getDescribtion());
        //if (product.getImage()!=null){
        //    Picasso.with(context).load(product.getImage()).into(holder.productImage);
        //}
        holder.productLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("product",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("productType","product");
                editor.commit();
                Intent intent = new Intent(context, ProductDetails.class);
                intent.putExtra("product_id",String.valueOf(product.getId()));
                intent.putExtra("productType","product");
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return listOfProducts.size();
    }
}
